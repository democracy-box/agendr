'use strict';
module.exports = (sequelize, DataTypes) => {
  const AgendaItem = sequelize.define('AgendaItem', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV1,
      type: DataTypes.UUID
    },
    title: DataTypes.TEXT,
    description: DataTypes.TEXT,
    length: DataTypes.INTEGER
  }, {tableName: 'agendaitems'});
  AgendaItem.associate = function(models) {
    AgendaItem.belongsTo(models.Agenda, {as: 'agenda', foreignKey: 'agendaId'});
    AgendaItem.hasMany(models.AgendaItemRanking, {as: 'rankings', foreignKey: 'itemId'});
    AgendaItem.beforeDestroy(async (item, options) => {
      const agenda = await models.Agenda.findById(item.agendaId);
      if (agenda.status !== "open") {
        throw new Error('Cannot delete item on agenda once voting has started.');
      }
    });
  };
  return AgendaItem;
};
