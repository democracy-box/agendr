import Sequelize, { Model } from 'sequelize';

export default (sequelize, DataTypes) => {
  var Agenda = sequelize.define('Agenda', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV1,
      type: DataTypes.UUID
    },
    title: DataTypes.TEXT,
    description: DataTypes.TEXT,
    length: DataTypes.INTEGER,
    status: {
      type: DataTypes.ENUM('open', 'voting', 'final'),
      allowNull: false,
      defaultValue: 'open'
    }
  }, {tableName: 'agendas'});
  Agenda.associate = function(models) {
    Agenda.Facilitator = Agenda.belongsTo(models.User, {as: 'facilitator', foreignKey: 'facilitatorId'});
    Agenda.Participants = Agenda.hasMany(models.AgendaParticipant, {as: 'participants', foreignKey: 'agendaId'});
    Agenda.Items = Agenda.hasMany(models.AgendaItem, {as: 'items', foreignKey: 'agendaId'});
    // TODO: possibly move this to AgendaParticipant model
    Agenda.afterUpdate((agenda, options) => {
      if (agenda.status === "voting") {
        return Promise.all([
          models.AgendaItem.findAll({where: {agendaId: agenda.id}, order: [['createdAt', 'ASC']]}),
          models.AgendaParticipant.findAll({where: {agendaId: agenda.id}, order: [['createdAt', 'ASC']]}),
          agenda
        ]).then(([items, participants, agenda]) => {
          // Delete any existing rankings (in the event we had an aborted vote earlier).
          return Promise.all(items.map((item) => models.AgendaItemRanking.destroy({where: {itemId: item.id}})))
            .then(() => {
              // Create initial rankings for all items for all participants.
              return Promise.all(participants.map((participant) => {
                let cumativeLength = 0;
                return Promise.all(items.map((item, idx) => {
                  cumativeLength += item.length;
                  return models.AgendaItemRanking.create({
                    agendaId: agenda.id,
                    participantId: participant.id,
                    itemId: item.id,
                    rank: idx,
                    approval: agenda.length >= cumativeLength
                  });
                }));
              }));
            });
        });
      }
    });
  };
  return Agenda;
};
