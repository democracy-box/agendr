'use strict';
module.exports = (sequelize, DataTypes) => {
  const AgendaParticipant = sequelize.define('AgendaParticipant', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV1,
      type: DataTypes.UUID
    },
    email: DataTypes.STRING,
    startDate: DataTypes.DATE,
    submitDate: DataTypes.DATE
  }, {tableName: 'agendaparticipants'});
  AgendaParticipant.associate = function(models) {
    AgendaParticipant.Agenda = AgendaParticipant.belongsTo(models.Agenda, {as: 'agenda', foreignKey: 'agendaId'});
    // TODO clean up the language here, make this consistent with GraphQL's agendaItemRankings property.
    AgendaParticipant.Votes = AgendaParticipant.hasMany(models.AgendaItemRanking, {as: 'votes', foreignKey: 'participantId'});
    // TODO move the code for generating all the participant initial rankings here ?
  };
  return AgendaParticipant;
};
