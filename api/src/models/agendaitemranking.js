'use strict';
module.exports = (sequelize, DataTypes) => {
  const AgendaItemRanking = sequelize.define('AgendaItemRanking', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV1,
      type: DataTypes.UUID
    },
    rank: DataTypes.INTEGER,
    approval: DataTypes.BOOLEAN
  }, {tableName: 'agendaitemrankings'});
  AgendaItemRanking.associate = function(models) {
    // associations can be defined here
    AgendaItemRanking.Voter = AgendaItemRanking.belongsTo(models.AgendaParticipant, {as: 'voter', foreignKey: 'participantId'});
    AgendaItemRanking.AgendaItem = AgendaItemRanking.belongsTo(models.AgendaItem, {as: 'item', foreignKey: 'itemId'});
  };
  return AgendaItemRanking;
};
