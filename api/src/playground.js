export default {
  settings: {
    "request.credentials": "include",
    "editor.cursorShape": "block"
  }
}
