import {ForbiddenError} from 'apollo-server';
import {Agenda, AgendaItem, AgendaItemRanking, AgendaParticipant} from '../models';
import { resolver } from 'graphql-sequelize';
import Sequelize, { Op } from 'sequelize';

// TODO not sure why `export default` doesn't work here.
module.exports = {
  Query: {
    agendaItem: resolver(AgendaItem),
    agendaItems: resolver(AgendaItem, {
      list: true
    })
  },
  AgendaItem: {
    result: async (agendaItem) => {
      // Get all completed votes for this item.
      const agenda = await Agenda.findById(agendaItem.agendaId,{
        include: [
          {
            model: AgendaItem,
            as: 'items'
          }
        ]
      });
      const itemRankings = await AgendaItemRanking.findAll({
        include: [
          {
            model: AgendaParticipant,
            as: 'voter',
            where: {
              submitDate: {
                [Op.gt]: 0
              }
            }
          }
        ],
        where: {
          itemId: agendaItem.id,
        }
      });

      if (agenda.status === "voting" || agenda.status === "final") {
        // Compute key result data metrics.
        // TODO figure out how to include 'rank' among these without recomputing all for every item.
        // idea: remove the rank altogether, return just the result data and let the agenda entityt take care of it...somehow.
        // the after property on the items resolver in agenda is probably a good place
        // TODO figure out best way to handle 0 based indexing of rankings through all these
        const mean = itemRankings.reduce((sum, itemRanking) => {
          return sum + itemRanking.get('rank');
        }, 0) / itemRankings.length;
        const median = itemRankings.length % 2 ?
              // We need to adjust for 0-indexed array, despite median usually being
              // `+1`.
              itemRankings[(itemRankings.length-1)/2].get('rank')
              :
              (itemRankings[(itemRankings.length/2)-1].get('rank') + itemRankings[(itemRankings.length/2)].get('rank')) / 2;
        const distribution = itemRankings.reduce((distribution, itemRanking) => {
          const rank = itemRanking.get('rank');
          const rankCount = distribution[rank] ? distribution[rank] + 1 : 1;
          distribution[rank] = rankCount;
          return distribution;
        }, Array(agenda.items.length).fill(0));
        const approvalCount = itemRankings.reduce((count, itemRanking) => {
          return count + (itemRanking.get('approval') ? 1 : 0);
        }, 0);
        return {
          mean,
          median,
          distribution,
          approvalCount
        };
      }
    }
  },
  Mutation: {
    addAgendaItem: (parent, {title, description, length, agendaId}) =>
      Agenda.findById(agendaId).then((agenda) => {
        if (agenda.status === 'open') {
          return AgendaItem.create({title, description, length, agendaId});
        }
        else {
          throw new ForbiddenError('Agenda is not open for adding items.');
        }
      }),
    deleteAgendaItem: async (parent, {agendaItemId}) => {
      let item = await AgendaItem.findById(agendaItemId);
      let agenda = await Agenda.findById(item.agendaId);
      await item.destroy();
      // TODO capture error if fails.
      await agenda.reload();
      return agenda;
    }
  }
};

