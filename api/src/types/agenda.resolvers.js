import {Agenda} from '../models';
import { resolver } from 'graphql-sequelize';

module.exports = {
  Query: {
    agenda: resolver(Agenda),
    allAgendas: async () => {
      const agendas = await Agenda.findAll()
      return agendas
    }
  },
  Agenda: {
    participants: resolver(Agenda.Participants),
    items: resolver(Agenda.Items)
  },
  Mutation: {
    createAgenda: (parent, {title, description, length}) =>
      Agenda.create({title, description, length}),
    setAgendaStatus: (parent, {agendaId, status}) =>
      Agenda.findById(agendaId)
      .then((agenda) =>
            agenda.update({status: status}, {where: {id: agendaId}}))
      .then(() =>
            Agenda.findById(agendaId))
  }
};
