import {User} from '../models';
import jwt from 'jsonwebtoken';
import slack from 'slack';

module.exports = {
  Query: {
    authenticationURI: (parent, {input: {service, redirectURI}}) => {
      let loginURI;
      console.log(service);
      // TODO use conf for client ID
      switch (service) {
      case "SLACK":
        const clientId = process.env.SLACK_CLIENT_ID;
        const baseURI = "https://slack.com/oauth/authorize?scope=identity.basic";
        const encodedRedirectURI = encodeURIComponent(redirectURI);
        const scopes = "identify";
        loginURI = `${baseURI}&client_id=${clientId}&redirect_uri=${encodedRedirectURI}&scopes=${scopes}`;
        break;
      case "GOOGLE":
        loginURI = "https://accounts.google.com/o/oauth2/v2/auth?client_id=961551543334-llvt22m5di41t67854l2l40775f1hfc4.apps.googleusercontent.com&response_type=id_token&scope=openid%20email&nonce=0394852-3190485-2490358&redirect_uri=" + encodeURIComponent(redirectURI);
        break;
      }
      return {
        service,
        loginURI,
        redirectURI
      }
    }
  },
  Mutation: {
    logInUserWithSlackOAuthCode: async (parent, {code, redirectURI}, context) => {
      if (code) {
        const accessRequestArgs = {
          client_id: process.env.SLACK_CLIENT_ID,
          client_secret: process.env.SLACK_CLIENT_SECRET,
          code,
          redirect_uri: redirectURI || process.env.SLACK_AUTH_REDIRECT_URI
        };
        console.log(accessRequestArgs);
        const slackAuthResponse = await slack.oauth
              .access(accessRequestArgs)
              .catch((err) => {
                console.log(err);
                throw new Error("Authentication verification failed");
              })
        const {access_token: accessToken, scope, user: {email}} = slackAuthResponse;
        const user = await User.findByEmail(email);
        // If user not found, create a new user with this information and load it.
        if (!user) {
          await User.create({
            email
          })
        }
        // Write slack-specific user data to db
        // TODO: implement slack user data model
        // Set authorization cookie to JWT with user ID.
        console.log(user);
        await context.logInUser(user)
        return user
      }
    },
    logInUserWithJWT: async (parent, {token}, context) => {
      try {
        // Validate the external token.
        // TODO support multiple potential integrations (do we need different callbacks?)
        const [err, token_data] = await jwt.verify(token);
        if (!token || err || !token_data.email_verified) {
          throw new Error("Invalid token.")
        }
        // Find user matching token ID data.
        // (Currently email).
        // TODO remove redirect_path.
        const {email, state: {redirect_path = "/"} = {}} = token_data;
        const user = await User.findByEmail(email);
        // If user not found, create a new user with this information and load it.
        if (!user) {
          await User.create({
            email
          })
        }
        // Set authorization cookie to JWT with user ID.
        await context.logInUser(user)
        return user
      }
      catch(err) {
        throw new Error(err)
      }
    }
  }
};
