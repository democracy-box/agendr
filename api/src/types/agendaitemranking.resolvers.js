import {ForbiddenError} from 'apollo-server';
import {Agenda, AgendaItem, AgendaItemRanking} from '../models';
import { resolver } from 'graphql-sequelize';
import Sequelize, { Op } from 'sequelize';

module.exports = {
  Query: {
    agendaItemRanking: resolver(AgendaItemRanking, {
      list: true,
      before: (findOptions, args) => {
        findOptions.order = [
          ['rank', 'ASC']
        ];
        return findOptions;
      }
    })
  },
  AgendaItemRanking: {
    agendaItem: resolver(AgendaItemRanking.AgendaItem),
  },
  Mutation: {
    // TODO: this should probably all be done on the client, which sends over a
    // list of updates to be committed in bulk.
    setAgendaItemRanking: async (parent, {id, rank}) => {
      let agendaItemRanking = await AgendaItemRanking.findById(id);
      // There are two cases:
      // Case 1: We're moving the item to a lower rank number.
      if (agendaItemRanking.rank > rank) {
        // In this case we increase the rank of all items from the new rank to the old rank by one.
        await AgendaItemRanking.update(
          {
            rank: Sequelize.literal('rank + 1')
          },
          {
            where: {
              participantId: agendaItemRanking.participantId,
              rank: {
                [Op.gte]: rank,
                [Op.lt]: agendaItemRanking.rank
              }
            }
          }
        );
      }
      // Case 2: We're moving the item to a higher rank number.
      else if (agendaItemRanking.rank < rank) {
        // In this case we decrease the rank of items between the old rank and the new rank.
        await AgendaItemRanking.update(
          {
            rank: Sequelize.literal('rank - 1')
          },
          {
            where: {
              participantId: agendaItemRanking.participantId,
              rank: {
                [Op.lte]: rank,
                [Op.gt]: agendaItemRanking.rank
              }
            }
          }
        );
      }
      else {
        return; // nothing to do.
      }
      // Then set the rank of this item to the new rank.
      await agendaItemRanking.update({ rank: rank});
      let itemRankings = await AgendaItemRanking.findAll({
          where: { participantId: agendaItemRanking.participantId },
          include: ['item'],
          order: [ ['rank', 'ASC'] ]
      });
      // Update the "approval" for each item (is it within the time limit of the
      // agenda?).
      if (itemRankings.length > 0) {
        let agenda = await Agenda.findById(itemRankings[0].item.agendaId);
        let {updatePromises} = itemRankings.reduce((state, itemRanking) => {
          state.cumulativeLength += itemRanking.item.length;
          state.updatePromises.push(itemRanking.update({
            approval: state.cumulativeLength <= agenda.length
          }));
          return state;
        }, {cumulativeLength: 0, updatePromises: []});
        await Promise.all(updatePromises);
        return itemRankings;
      }
      else {
        throw new ForbiddenError('No items in agenda.');
      }
    }
  }
}
