import {ForbiddenError} from 'apollo-server';
import {Agenda, AgendaParticipant} from '../models';
import { resolver } from 'graphql-sequelize';

module.exports = {
  Query: {
    agendaParticipant: resolver(AgendaParticipant),
    agendaParticipants: resolver(AgendaParticipant, {
      list: true
    })
  },
  AgendaParticipant: {
    agenda: resolver(AgendaParticipant.Agenda),
    agendaItemRankings: resolver(AgendaParticipant.Votes, {
      before: (findOptions, args, context) => {
        findOptions.order = [
          ['rank', 'ASC']
        ];
        return findOptions;
      }
    })
  },
  Mutation: {
    addAgendaParticipant: (parent, {email, agendaId}) =>
      Agenda.findById(agendaId).then((agenda) => {
        if (agenda.status === 'open') {
          return AgendaParticipant.create({email, agendaId});
        }
        else {
          throw new ForbiddenError('Agenda is not open for adding participants.');
        }
      }),
    setAgendaParticipantStartDate: (parent, {id, startDate}) =>
      AgendaParticipant.findById(id)
      .then((participant) =>
            Agenda.findById(participant.agendaId)
            .then((agenda) => {
              if (agenda.status === 'voting') {
                // TODO add check to make sure the start date hasn't already been set
                return participant.update({startDate: startDate}, {where: {id: id}})
                  .then(() => AgendaParticipant.findById(id));
              } 
            })
           ),
    setAgendaParticipantSubmitDate: (parent, {id, submitDate}) =>
      AgendaParticipant.findById(id)
      .then((participant) =>
            Agenda.findById(participant.agendaId)
            .then((agenda) => {
              if (agenda.status === 'voting') {
                // TODO add check to make sure the submit date hasn't already been set
                return participant.update({submitDate: submitDate}, {where: {id: id}})
                  .then(() => AgendaParticipant.findById(id));
              } 
            })
           ),
  }
}
