import glob from 'glob';

const resolverFiles = glob.sync(`**/*.resolvers.js`, { absolute: true });

export const resolvers = resolverFiles.reduce((resolvers, resolverFile) => {
  const fileResolvers = require(resolverFile);
  Object.entries(fileResolvers).forEach(([key, item]) => {
    resolvers[key] = Object.assign(resolvers[key] || {}, fileResolvers[key] || {});
  }); 
  return resolvers;
}, {});
