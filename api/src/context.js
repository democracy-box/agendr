import {logInUser} from "./auth";

export default ({res: response, req: request}) => (
  {
    currentUser: request.user,
    logInUser: async (user) => await logInUser(response, user),
    cookies: request.cookies
  }
)
