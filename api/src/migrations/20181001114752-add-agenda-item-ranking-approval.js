'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('agendaitemrankings', 'approval', {
        type: Sequelize.BOOLEAN,
      }),
    ];
  },

  down: (queryInterface, Sequelize) => {
    return [
      queryInterface.removeColumn('agendaitemrankings', 'approval'),
    ];
  }
};
