'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('agendas', 'status', {
        allowNull: false,
        type: Sequelize.ENUM,
        values: ['open', 'voting', 'final'],
        defaultValue: 'open'
      }),
      queryInterface.sequelize.query("UPDATE agendas SET status='open' WHERE status IS NULL")
    ];
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('agendas', 'status')
      .then(() => queryInterface.sequelize.query('DROP TYPE "enum_agendas_status";'));
  }
};
