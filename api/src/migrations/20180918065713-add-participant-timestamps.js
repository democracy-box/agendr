'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('agendaparticipants', 'startDate', {
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('agendaparticipants', 'submitDate', {
        type: Sequelize.DATE,
      }),
    ];
  },

  down: (queryInterface, Sequelize) => {
    return [
      queryInterface.removeColumn('agendaparticipants', 'startDate'),
      queryInterface.removeColumn('agendaparticipants', 'submitDate'),
    ];
  }
};
