'use strict';
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('agendaitemrankings', {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV1,
        type: DataTypes.UUID
      },
      rank: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      participantId: {
        allowNull: false,
        type: DataTypes.UUID,
        references: { model: 'agendaparticipants', key: 'id' }
      },
      itemId: {
        allowNull: false,
        type: DataTypes.UUID,
        references: { model: 'agendaitems', key: 'id' }
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },
  down: (queryInterface, DataTypes) => {
    return queryInterface.dropTable('agendaitemrankings');
  }
};
