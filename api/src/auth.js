import jwtMiddleware from 'express-jwt'
import jwt from 'jsonwebtoken'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'

import { User, sequelize } from './models'

const {
  SESSION_SECRET
} = process.env;

const signJWT = (payload, secretOrPrivateKey, options={}) => new Promise(
  (resolve, reject) =>
    jwt.sign(payload, secretOrPrivateKey, options, (err, token) => {
      if (err) {
        reject(err)
      } else {
        resolve(token)
      }
    }))

/**
 * Takes a user as an argument.
 * Sets a JWT token in a server-side cookie for the session.
*/
export const logInUser = async (response, {id, username}) => {
  const token = await signJWT({id}, SESSION_SECRET, {expiresIn: "30 days"})
  response.cookie("jwt", token, {httpOnly: true})
}

export const setupAuth = (app) => {
  app.use(cookieParser())
  app.use(jwtMiddleware({
    secret: SESSION_SECRET,
    credentialsRequired: false,
    getToken: (req) => req.cookies.jwt
  }))
  app.post('/login', bodyParser.urlencoded(), async (request, response) => {
    const {username, password} = request.body
    const user = await User.findByUsername(username)
    if (await user.verifyPassword(password)) {
      await logInUser(response, user)
      response.redirect(302, "/")
    } else {
      response.redirect(302, "/?error=login_failed")
    }
  })
  app.get('/oauth/slack', bodyParser.urlencoded(), async (request, response, next) => {
    try {
      const {token = null} = request.params
      console.log(request.params)
      console.log(token)
      // TODO support multiple potential integrations (do we need different callbacks?)
      const [err, token_data] = await jwt.verify(token);
      if (!token || err || !token_data.email_verified) {
        throw new Error("Invalid token.")
      }
      const {email, state: {redirect_path = "/"} = {}} = token_data;
      const user = await User.findByEmail(email);
      await logInUser(response, user)
      response.redirect(302, redirect_path)
    }
    catch(err) {
      response.redirect(302, "/?error=login_failed")
    }
  })
  app.get('/openid/google', bodyParser.urlencoded(), async (request, response, next) => {
    try {
      const {token = null} = request.params
      console.log(request.params)
      console.log(token)
      // TODO support multiple potential integrations (do we need different callbacks?)
      const [err, token_data] = await jwt.verify(token);
      if (!token || err || !token_data.email_verified) {
        throw new Error("Invalid token.")
      }
      const {email, state: {redirect_path = "/"} = {}} = token_data;
      const user = await User.findByEmail(email);
      await logInUser(response, user)
      response.redirect(302, redirect_path)
    }
    catch(err) {
      response.redirect(302, "/?error=login_failed")
    }
  })
  app.post('/register', bodyParser.urlencoded(), async (request, response) => {
    const {username, password} = request.body
    const user = await User.register({username}, password)
    await logInUser(response, user)
    response.redirect(302, "/")
  })
}
