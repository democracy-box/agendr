import express from 'express'

import { ApolloServer } from 'apollo-server-express'
import context from './context'
import playground from './playground'
import { schema as typeDefs } from 'agendr-schema';
import { resolvers } from './types'

import {setupAuth} from './auth'

const app = express();

setupAuth(app)

export const apolloServer = new ApolloServer({
  typeDefs, resolvers, context, playground
})
apolloServer.applyMiddleware({ app })

export default app
