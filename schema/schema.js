'use strict';
module.exports = require('graphql-tag')`
type User {
  id: ID
  email: String
  firstName: String
  lastName: String
}

enum AgendaStatus {
  open
  voting
  final
}

type Agenda {
  id: ID!
  owner: User
  title: String!
  description: String!
  length: Int!
  participants: [AgendaParticipant]
  items: [AgendaItem]
  status: AgendaStatus
}

# TODO do we need an ID on this child element? If so, how?
type AgendaItemResult {
  id: ID!
  rank: Int
  mean: Float
  median: Float # Can be an average of vals in case of even number of votes.
  approvalCount: Int
  distribution: [Int]
}

# TODO QUESTION Shouldn't this be Agenda? Didn't work on first try, what's the right way?
type AgendaItem {
  id: ID
  title: String
  length: Int
  description: String
  result: AgendaItemResult
}

# TODO Rename to Ballot, relate to user
type AgendaParticipant {
  id: ID
  # user: User
  # TODO this should be a user ref, not an email string
  email: String
  # agenda: Agenda
  agenda: Agenda,
  agendaItemRankings: [AgendaItemRanking],
  startDate: Int,
  submitDate: Int
}

# TODO rename to BallotVoteEdge
type AgendaItemRanking {
  id: ID
  date: String
  rank: Int
  approval: Boolean
  agendaItem: AgendaItem
  agenda: Agenda
  participant: AgendaParticipant
}

enum TabulationMode {
  IRV
  AVERAGE
}

enum AuthenticationService {
  SLACK
  GOOGLE
  # GITHUB
  # BITBUCKET
  # FACEBOOK
}

type AuthenticationURI {
  service: AuthenticationService
  loginURI: String
  redirectURI: String
}

input AuthenticationURIInput {
  service: AuthenticationService
  redirectURI: String
}


type Query {
  agenda(id: ID!): Agenda
  allAgendas: [Agenda]
  allAgendaItems: [AgendaItem]
  agendaItem(id: ID!): AgendaItem
  agendaItems(agendaId: ID!): [AgendaItem]
  agendaParticipant(id: ID!): AgendaParticipant # Will also init participants ballot
  agendaParticipants(agendaId: ID!): [AgendaParticipant]
  agendaParticipantUsers(id: ID!): [User]
  agendaItemRanking(id: ID!): AgendaItemRanking
  agendaItemRankings(agendaId: ID, participantId: ID, agendaItemId: ID): [AgendaItemRanking]
  agendaRankingResult(agendaId: ID!, mode: TabulationMode): [AgendaItem] # TODO create a type to hold info about rank tally results
  # TODO add queries to get all votes, grouped by participant, to act as a checksum.
  authenticationURI(input: AuthenticationURIInput!): AuthenticationURI
  userForJWT(jwt: String!): User
}

type Mutation {
  addUser(email: String, firstName: String, lastName: String): User
  logInUserWithJWT(token: String): User
  logInUserWithSlackOAuthCode(code: String!, redirectURI: String): User
  createAgenda(title: String!, description: String!, length: Int, facilitator: ID): Agenda
  setAgendaStatus(agendaId: ID!, status: String!): Agenda
  # addAgendaParticipants(id: ID!, participants: [User]): Agenda
  # TODO should this return an Agenda or and AgendaItem?
  addAgendaItem(agendaId: ID!, title: String!, description: String!, length: Int!): AgendaItem,
  deleteAgendaItem(agendaItemId: ID!): Agenda,
  addAgendaParticipant(agendaId: ID!, email: String!): AgendaParticipant
  deleteAgendaParticipant(agendaParticipantId: ID!): Agenda,
  setParticipantAgendaItemRank(participantId: ID!, agendaItemId: ID!, rank: Int): [AgendaItemRanking] # Returns full list
  setAgendaItemRanking(id: ID!, rank: Int): [AgendaItemRanking] # Returns full list
  setAgendaParticipantStartDate(id: ID!, startDate: Int!): AgendaParticipant
  setAgendaParticipantSubmitDate(id: ID!, submitDate: Int!): AgendaParticipant
  # addAgendaItems(id: ID!, items: [AgendaItem]): Agenda
  # addAgendaRankVote(agenda: Agenda, voter: User, agendaitems: [AgendaItem]): Agenda
}
`;
