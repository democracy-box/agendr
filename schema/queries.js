import gql from 'graphql-tag';

export const AllAgendaItems = gql`
query allAgendaItems {
  allAgendaItems {
    id
    title
    description
    length
  }
}
`

export const Agenda = gql`
query Agenda($id: ID!) {
  agenda(id: $id) {
    id
    title
    description
    length
  }
}
`

export const AgendaItems = gql`
  query agendaItems($agendaId: ID!) {
    agendaItems(agendaId: $agendaId) {
      id
      title
      description
      length
    }
  }
`

export const AgendaParticipant = gql`
query agendaParticipant($id: ID!) {
  agendaParticipant(id: $id) {
    id
    email
    status
    agenda {
      id
      meetingLength: length
      title
      description
    }
    agendaItemRankings {
      id
      rank
      approval
      agendaItem {
        id
        title
        description
        length
      }
    }
  }
}
`

export const AgendaParticipants = gql`
query agendaParticipants($agendaId: ID!) {
  agendaParticipants(agendaId: $agendaId) {
    id
    email
    status
    agendaId
  }
}
`

export const CreateAgenda = gql`
mutation CreateAgenda($title: String!, $description: String!, $length: Int, $facilitator: ID) {
  createAgenda(title: $title, description: $description, length: $length, facilitator: $facilitator) {
    id
    title
    description
    length
  }
}
`

export const AddAgendaItem = gql`
mutation AddAgendaItem($agendaId: ID!, $title: String!, $description: String!, $length: Int!) {
  addAgendaItem(agendaId: $agendaId, title: $title, description: $description, length: $length) {
    id
    title
    description
    length
    agendaId
  }
}
`

export const AddAgendaParticipant = gql`
mutation addAgendaParticipant($agendaId: ID!, $email: String!) {
  addAgendaParticipant(agendaId: $agendaId, email: $email) {
    id
    # TODO this should be a user ref, not an email string.
    email
    # TODO refactor so this is a full nested  object, as below
    # agendaId
    agenda {
      id
    }
    # user {
    #   id
    #   email
    # }
  }
}
`

export const GetLocalRankedAgendaItems = gql`
query GetLocalRankedAgendaItems($agendaId: ID!, $participantId: ID!) {
  rankedAgendaItems(agendaId: $agendaId, participantId: $participantId) @client {
    id
    rank
    approval
    agendaItemId
  }
}
`

// # NOTE that approval is not set directly. Rank is specified and the server
// # determines approval based on meeting length.
export const SetLocalParticipantAgendaItemRank = gql`
mutation setLocalRankedAgendaItemRank($agendaId: ID!, $participantId: ID!, $agendaItemId: ID!, $rank: Int!) {
  setParticipantAgendaItemRank(agendaId: $agendaId, participantId: $participantId, agendaItemId: $agendaItemId, rank: $rank) @client {
    id
    participantId
    agendaId
    agendaItemId
    rank
    approval
  }
}
`

export const GetRankedAgendaItems = gql`
query GetRankedAgendaItems($agendaId: ID!, $participantId: ID!) {
  rankedAgendaItems(agendaId: $agendaId, participantId: $participantId) {
    id
    rank
    approval
    agendaItemId
  }
}
`

export const SetParticipantAgendaItemRank = gql`
mutation setParticipantAgendaItemRank($participantId: ID!, $agendaItemId: ID!, $rank: Int!) {
  setRankedAgendaItemRank(participantId: $participantId, agendaItemId: $agendaItemId, rank: $rank) {
    id
    rank
    approval
    agendaItem
  }
}
`

export const SetRankedAgendaItemRank = gql`
mutation setRankedAgendaItemRank($id: ID!, $rank: Int!) {
  setRankedAgendaItemRank(id: $id, rank: $rank) {
    id
    rank
    approval
    agendaItem {
      id
      title
      description
      length
    }
  }
}
`

export const SetAgendaParticipantStatus = gql`
mutation setAgendaParticipantStatus($id: ID!, $status: BallotStatus!) {
  setAgendaParticipantStatus(id: $id, status: $status) {
    id
    status
  }
}
`
