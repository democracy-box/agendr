# AgendR

AgendR is a tool to plan group agendas using ranked voting. Enter the agenda
topics, lengths, and meeting times and invite others to vote. Once all votes are
in, you get an agenda that incporporates everyone's prioritization.


## rex

AgendR is build on rex. rex is a Really EXcellent way to develop webapps.

## Pre-requisites

First, (install Docker)[https://docs.docker.com/install/].

We're going to assume you also have NodeJS and yarn installed globally.

## Setup

There are a few slight tricks to getting the system working on your first
install. You'll need to install the packages in the API from the host, not
within the docker api instance, because the API dir is mounted one level below
the full project dir, where the schema lives.

This is fairly simple, just `cd api; yarn install`

Once you've installed the app, you also need to upgrade the agendr-schema
package from the host in the API dir whenever it changes. To do this run `yarn
upgrade agendr-schema` from the API dir. If the app is not working for you after
an upgrade, this is a likely cause and solution.

## Developing

### UI Development with Storybook

To develop just the UI, you can run storybook locally without needing all the
API setup, etc. via docker. Just run `npm run storybook` from within the
ui dir to start storybook and preview components at
(localhost:9009)[http://localhost:9009/].

### Full-stack development with Docker

To set up the full stack for local development, run

```
docker-compose -f docker-compose-dev.yml up
```

To get a development environment. Visit
(http://localhost:8080)[http://localhost:8080] to see your running app.

## Backlog / Feature Suggestions

* Multiple participants can add agenda item to a shared meeting
* Coaching around how to best structure meetings (Biggest first, not easy ones)
* Agenda timer and notifications
* Integrate with Stack-In tracker
* Create loomio post for any agenda item ("bump to loomio")
* Bump to next meeting
