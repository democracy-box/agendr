const path = require("path");

module.exports = (baseConfig, env, defaultConfig) => {
  // Add alias for components, containers, and bindings dirs.
  defaultConfig.resolve.alias = {
    ...defaultConfig.resolve.alias,
    '@components': path.resolve(__dirname, '../src/components/'),
    '@containers': path.resolve(__dirname, '../src/containers/'),
    '@bindings': path.resolve(__dirname, '../src/bindings/'),
    '@common': path.resolve(__dirname, '../src/common/'),
  }; 
  return defaultConfig;
};
