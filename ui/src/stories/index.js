import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container } from 'semantic-ui-react';

import { setOptions } from '@storybook/addon-options';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { themes } from '@storybook/components';

import withGraphQL from './with-graphql.js';

// import typeDefs from 'agendr-schema';
import { getStoryRouterCreateAgenda, getStoryRouterVote, getStoryRouterLogIn } from './storyrouter';

import { Button, Welcome } from '@storybook/react/demo';

// Import individual components.
import Agenda from 'components/Agenda'
import AgendaItemList from 'components/AgendaItemList'
import AgendaParticipantList from 'components/AgendaParticipantList'
import AgendaForm from 'components/AgendaForm'
import AgendaParticipantsForm from 'components/AgendaParticipantsForm'
import AgendaItemForm from 'components/AgendaItemForm'
import AgendaStartVoteForm from 'components/AgendaStartVoteForm'
import AgendaSteps from 'components/AgendaSteps'
import RankingForm from 'components/RankingForm'
import ParticipantStartVoteForm from 'components/ParticipantStartVoteForm'
import ParticipantSubmitVoteForm from 'components/ParticipantSubmitVoteForm'
import ItemRankTable from 'components/ItemRankTable'
import ResultItemReport from 'components/ResultItemReport'

// Import bindings.
import BoundAgendaItemList from 'bindings/AgendaItemList'
import BoundParticipantStartVoteForm from 'bindings/ParticipantStartVoteForm'
import BoundParticipantSubmitVoteForm from 'bindings/ParticipantSubmitVoteForm'
import BoundItemRankTable from 'bindings/ItemRankTable'
import BoundResultItemReport from 'bindings/ResultItemReport'

// Import contianers.
import AgendaCreate from 'containers/AgendaCreate'
import ParticipantAgendaVote from 'containers/ParticipantAgendaVote'

// Import stories loaded as modules.
import authenticationStories from './authentication.stories'

import * as mock from './mock-data';

setOptions({
  name: 'AgendR Storybook',
  hierarchySeparator: /\/|\./,
  hierarchyRootSeparator: /\|/,
  showAddonPanel: false,
});

class DevtoolsIFrameFix extends React.Component {
  constructor() {
    super();
    if (window.parent !== window) {
      window.parent.__APOLLO_CLIENT__ = window.__APOLLO_CLIENT__;
    }
  }

  render() {
    return React.Children.only(this.props.children);
  }
}

function withDevtoolsIFrameFix() {
  return (story) => {
    return <DevtoolsIFrameFix>{story()}</DevtoolsIFrameFix>;
  };
}


storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('UI|Components/AgendaSteps', module)
  .add('default', () =>
      <AgendaSteps />
      )
  .add('step 2', () =>
       <AgendaSteps activeStep="2" />
      )
  .add('step 3', () =>
       <AgendaSteps activeStep="3" />
      )
  .add('step 4', () =>
       <AgendaSteps activeStep="4" />
      );

storiesOf('UI|Components/AgendaParticipantList', module)
  .addDecorator(getStoryRouterCreateAgenda())
  .add('Example List', () =>
       <AgendaParticipantList participants={mock.participants} />
      );

storiesOf('UI|Components/AgendaItemList', module)
  .addDecorator(getStoryRouterVote())
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agenda: () => mock.agendaWithItems
      });
    },
    Mutation: () => ({
      deleteAgendaItem: (vars) => {
        console.log(vars);
        return {
          ...mock.agendaWithItems,
          items: mock.agendaWithItems.items.slice(1)
        };
      }
    })
  }))
  .add('Basic List', () =>
       <AgendaItemList meetingLength="45" items={mock.agendaItems} />
      )
  .add('Editable List', () =>
       <AgendaItemList meetingLength="45" items={mock.agendaItems} editable />
      )
  .add('Bound', () =>
       <BoundAgendaItemList agendaId="1" />
      );

storiesOf('UI|Components/AgendaStartVoteForm', module)
  .add('Open', () =>
       <AgendaStartVoteForm agendaStatus="open" />
      )
  .add('Voting', () =>
       <AgendaStartVoteForm agendaStatus="voting" />
      )
  .add('Final', () =>
       <AgendaStartVoteForm agendaStatus="final" />
      );

storiesOf('UI|Components/ParticipantStartVoteForm', module)
  .addDecorator(getStoryRouterVote())
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agendaParticipant: () => mock.participantFull,
      });
    },
    Mutation: () => ({
      setAgendaParticipantStartDate: () => ({
        id: 1,
        startDate: true
      })
    })
  }))
  .add('not started', () =>
       <ParticipantStartVoteForm startDate={null} />
      )
  .add('started', () =>
       <ParticipantStartVoteForm startDate="1"/>
      )
  .add('bound', () =>
       <BoundParticipantStartVoteForm participantId="1"/>
      );

storiesOf('UI|Components/ParticipantSubmitVoteForm', module)
  .addDecorator(getStoryRouterVote())
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agendaParticipant: () => mock.participantFull,
      });
    },
    Mutation: () => ({
      setAgendaParticipantSubmitDate: () => ({
        id: 1,
        submitDate: true
      })
    })
  }))
  .add('not submitted', () =>
       <ParticipantSubmitVoteForm submitDate={null}/>
      )
  .add('submitted', () =>
       <ParticipantSubmitVoteForm submitDate="1"/>
      )
  .add('bound', () =>
       <BoundParticipantSubmitVoteForm participantId="1"/>
      );

storiesOf('UI|Components/ItemRankTable', module)
  .addDecorator(withDevtoolsIFrameFix())
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agendaParticipant: () => mock.participantFull,
      });
    },
    Mutation: () => ({
      setRankedAgendaItemRank: () => mock.agendaItemRankingsReordered
    })
  }))
  .add('Basic List', () =>
       <ItemRankTable meetingLength="45" items={mock.agendaItemRankings} />
      )
  .add('Bound List', () =>
       <BoundItemRankTable participantId="1"/>
      );

storiesOf('UI|Components/ResultItemReport', module)
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agenda: () => mock.agendaWithItems,
      });
    }
  }))
  .add('Basic Report', () =>
       <ResultItemReport items={mock.resultItems}/>
      )
  .add('Bound Report', () =>
       <BoundResultItemReport agendaId="1"/>
      );

storiesOf('UI|Forms/Agenda Forms', module)
  .addDecorator(getStoryRouterCreateAgenda())
  .add('new agenda', () =>
       <AgendaForm />
      );

storiesOf('UI|Forms/Agenda Item Forms', module)
  .addDecorator(getStoryRouterCreateAgenda())
  .add('new agenda item', () =>
      <AgendaItemForm />
      );

storiesOf('UI|Groups/Agenda', module)
  .add('basic, with agenda items', () =>
       <Agenda
         title="CoLab Coop AgendR Meeting"
         length="45"
         description="A really important meeting to discuss the process for deciding what we should meet on at future meetings."
         items={mock.agendaItems}
         participants={mock.participants}
         showItems
         showParticipants
       />
      );

storiesOf('Screens|Create Agenda', module)
  .addDecorator(withGraphQL({
    Mutation: () => ({
      createAgenda: () => mock.agendaNew
    })
  }))
  .addDecorator(getStoryRouterCreateAgenda())
  .add('1: New', () =>
       <AgendaCreate />
      );

storiesOf('Screens|Create Agenda', module)
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agenda: () => mock.agendaWithParticipants,
        agendaParticipants: () => mock.participantsOne
      });
    },
    Mutation: () => ({
      addAgendaParticipant: () => mock.participantsTwo[1]
    })
  }))
  .addDecorator(getStoryRouterCreateAgenda('/1/add-participants'))
  .add('2: Add Participants', () =>
       <AgendaCreate />
      );

storiesOf('Screens|Create Agenda', module)
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agenda: () => mock.agendaWithParticipants,
        agendaParticipants: () => mock.participants,
        agendaItems: () => mock.agendaItems1
      });
    },
    Mutation: () => ({
      addAgendaItem: () => mock.agendaItems3[1]
    })
  }))
  .addDecorator(getStoryRouterCreateAgenda('/1/add-items'))
  .add('3: Add Items', () =>
       <AgendaCreate />
      )

storiesOf('Screens|Create Agenda', module)
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agenda: () => mock.agendaWithItems,
        agendaParticipants: () => mock.participants,
        agendaItems: () => mock.agendaItems1
      });
    },
    Mutation: () => ({
      addAgendaItem: () => mock.agendaItems3[1]
    })
  }))
  .addDecorator(getStoryRouterCreateAgenda('/1/start-vote'))
  .add('4: Finalize', () =>
       <AgendaCreate />
      )
  .add('5: See Results', () =>
       <Container>
       <SegmentGroup>
       <AgendaSteps activeStep="5" />
       <Segment align="center" attached>
       <Container text>
       <p>Voting has started for this agenda. The current result order is shown below.</p>
       </Container>
       </Segment>
       <Segment attached>
       <Agenda
       title="CoLab Coop AgendR Meeting"
       meetingLength="45"
       items={mock.agendaItems3}
       />
       </Segment>
       </SegmentGroup>
       </Container>
      );

storiesOf('Screens|Vote', module)
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agenda: () => mock.agendaWithParticipants,
        agendaParticipant: () => mock.participants[0],
        agendaParticipants: () => mock.participants,
        agendaItems: () => mock.agendaItems1
      });
    },
    Mutation: () => ({
      addAgendaItem: () => mock.agendaItems3[1]
    })
  }))
  .addDecorator(getStoryRouterVote('/1'))
  .add('Rank and submit', () =>
       <ParticipantAgendaVote />
      );
storiesOf('Screens|Vote', module)
  .addDecorator(getStoryRouterVote('/confirm-submit'))
  .add('Confirm Vote', () =>
       <ParticipantAgendaVote />
      );
storiesOf('Screens|Vote', module)
  .addDecorator(getStoryRouterVote('/confirm-decline'))
  .add('Confirm Decline', () =>
       <ParticipantAgendaVote />
      );
  // .add('Confirm', () =>
  //      <Container>
  //        <Segment align="center" attached>
  //          <Container text>
  //            <h2>Vote Submitted</h2>
  //            <p>See the results here</p>
  //          </Container>
  //        </Segment>
  //      </Container>
  //     )
  // .add('Abstain', () =>
  //      <Container>
  //      <Segment align="center" attached>
  //      <Container text>
  //      <h2>You Have Abstained</h2>
  //      <p>You can see the reuslts of the ranking here.</p>
  //      </Container>
  //      </Segment>
  //      </Container>
  //     );


storiesOf('Bindings|AgendaItemList', module)
  .addDecorator(withGraphQL({
    Query: () => {
      return ({
        agendaItems: () => {
          return mock.agendaItems3;
        }
      });
    },
    // Mutation: () => ({
    //   addUnit: () => unit4
    // })
  }))
  .add('default', () =>
    <BoundAgendaItemList agendaId="1"/>
      );

authenticationStories();
