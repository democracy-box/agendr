export const agendaItems = [
  {
    agendaId: 1,
    id: 1,
    title: "Using Agendr for all our meetings",
    description: "Agendr is a new tool for making agendas as a group. Let's try it!",
    length: 15,
    result: {
      rank: 3,
      mean: 3,
      median: 3,
      approvalCount: 0,
      distribution: [0,0,4]
    },
    xlinks: [
      {
        url: "#",
        text: "Link to external resource"
      }
    ]
  },
  {
    id: 2,
    title: "The best heart to heart share ever",
    description: "",
    length: 30,
    agendaId: 1,
    result: {
      rank: 1,
      mean: 1.25,
      median: 1,
      approvalCount: 4,
      distribution: [3,1,0]
    }
  },
  {
    id: 3,
    title: "Really important budget work we don't want to do",
    length: 30,
    agendaId: 1,
    result: {
      rank: 2,
      mean: 1.75,
      median: 2,
      approvalCount: 4,
      distribution: [1,3,0]
    }
  }
];

export const agendaItems1 = agendaItems.slice(0,1);
export const agendaItems2 = agendaItems.slice(0,2);
export const agendaItems3 = agendaItems.slice(0,3);

export const participants = [
  {
    id: 11,
    email: "ethan@colab.coop",
    agendaId: 1,
    startDate: false,
    submitDate: false
  },
  {
    id: 12,
    email: "colabr@colab.coop",
    agendaId: 1,
    startDate: false,
    submitDate: false
  }
];

export const participantsOne = participants.slice(0,1);
export const participantsTwo = participants.slice(0,2);

export const agendaNew = {
  id: "1",
  title: "CoLab Collaborator Meeting",
  description: "A time for us to gather and collectively decide on matters that impact us all.",
  length: 45,
  status: "open",
  participants: [],
  items: []
}

export const agendaWithParticipants = {
  ...agendaNew,
  participants: participants
}

export const agendaWithItems = {
  ...agendaWithParticipants,
  items: agendaItems
}

export const agendaItemRankings = agendaItems.map((item, idx) => {
  return {
    id: idx+100,
    rank: idx,
    approval: (idx < 2),
    agendaItem: {
      ...item
    }
  };
});

export const agendaItemRankingsReordered = agendaItemRankings.slice(1).concat(agendaItemRankings.slice(0,1)).map((item, idx) => ({...item, rank: idx, approval: (idx < 2)}));

export const participantFull = {
  ...participantsOne,
  startDate: null,
  submitDate: null,
  agenda: {
    ...agendaNew
  },
  agendaItemRankings: agendaItemRankings
};

export const resultItems = agendaItems.sort((a, b) => {
  return a.result.rank < b.result.rank ? -1 : 1;
});
