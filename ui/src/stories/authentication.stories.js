import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { storiesOf } from '@storybook/react';
import { getStoryRouterWithLocation } from './storyrouter';
import { getStoryRouterCreateAgenda, getStoryRouterVote, getStoryRouterLogIn } from './storyrouter';
import withGraphQL from './with-graphql';

import { linkTo } from '@storybook/addon-links';

import { Segment, Container } from 'semantic-ui-react';

import LogInForm from 'components/LogInForm'
import RegistrationForm from 'components/RegistrationForm'
import LogInModal from 'components/LogInModal'
import RegistrationModal from 'components/RegistrationModal'
import OAuthButton from 'components/OAuthButton'
import BoundOAuthButton from 'bindings/OAuthButton'
import OAuthButtons from 'components/OAuthButtons'

const routerAuthentication = (path) => getStoryRouterWithLocation({
  '/login': linkTo('UI|Forms/LogInForm', 'default'),
  '/register': linkTo('UI|Forms/RegistrationForm', 'default')
})

export default () => {
  storiesOf('UI|Components/OAuthButtons', module)
    .addDecorator(routerAuthentication())
    .addDecorator(withGraphQL({
      Query: () => {
        const redirectURI = "http://localhost:9009/iframe.html?selectedKind=UI%7CComponents%2FOAuthButtons&selectedStory=all";
        return ({
          authenticationURI: (root, {input: {service, redirectURI}}) => {
            switch (service) {
            case "SLACK":
              return {
                  "service": "SLACK",
                  "loginURI": "https://slack.com/oauth/authorize?scope=identity.basic%20identity.email&client_id=40713242068.748154335095&redirect_uri=http%3A%2F%2Flocalhost%3A9009%2F%3FselectedKind%3DUI%257CComponents%252FOAuthButtons%26selectedStory%3Dbound%2520slack%26full%3D0%26addons%3D0%26stories%3D1%26panelRight%3D0%26addonPanel%3Dstorybook%252Factions%252Factions-panel&scopes=identify",
                  "redirectURI": "http://localhost:9009/?selectedKind=UI%7CComponents%2FOAuthButtons&selectedStory=bound%20slack&full=0&addons=0&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel",
                  "__typename": "AuthenticationURI"
              };
              break;
            case "GOOGLE":
              return ({
                service: "GOOGLE",
                loginURI: "https://accounts.google.com/o/oauth2/v2/auth?client_id=961551543334-llvt22m5di41t67854l2l40775f1hfc4.apps.googleusercontent.com&response_type=id_token&scope=openid%20email&nonce=0394852-3190485-2490358&redirect_uri=" + encodeURIComponent(redirectURI),
                // loginURI: "https://accounts.google.com/o/oauth2/v2/auth?client_id=961551543334-llvt22m5di41t67854l2l40775f1hfc4.apps.googleusercontent.com&response_type=id_token&scope=openid%20email&nonce=0394852-3190485-2490358&redirect_uri=" + encodeURIComponent('http://localhost:8088/openid/google'),
                redirectURI: "http://localhost:9009/"
              });
              break
            };
          }
        });
      }
    }))
    .add('all', () =>
         <OAuthButtons services={["GOOGLE", "FACEBOOK", "BITBUCKET", "GITHUB"]} redirectURI="http://localhost9009" />
        )
    .add('slack', () =>
         <OAuthButton service="SLACK">Log In with Slack</OAuthButton>
        )
    .add('google', () =>
         <OAuthButton service="GOOGLE">Log In with Google</OAuthButton>
        )
    .add('facebook', () =>
         <OAuthButton service="FACEBOOK">Log In with Facebook</OAuthButton>
        )
    .add('github', () =>
         <OAuthButton service="GITHUB">Log In with Github</OAuthButton>
        )
    .add('bitbucket', () =>
         <OAuthButton service="BITBUCKET">Log In with BitBucket</OAuthButton>
        )
    .add('bound slack', () =>
         <BoundOAuthButton service="SLACK" redirectURI="http://localhost:9009/?selectedKind=UI%7CComponents%2FOAuthButtons&selectedStory=bound%20slack&full=0&addons=0&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel">Log In with Slack</BoundOAuthButton>
        )
    .add('bound google', () =>
         <BoundOAuthButton service="GOOGLE" redirectURI="http://localhost:9009/">Log In with Google</BoundOAuthButton>
        )
    .add('bound facebook', () =>
         <BoundOAuthButton service="FACEBOOK" redirectURI="http://localhost:9009/">Log In with Facebook</BoundOAuthButton>
        )
    .add('bound github', () =>
         <BoundOAuthButton service="GITHUB" redirectURI="http://localhost:9009/">Log In with Github</BoundOAuthButton>
        )
    .add('bound bitbucket', () =>
         <BoundOAuthButton service="BITBUCKET" redirectURI="http://localhost:9009/">Log In with Bitbucket</BoundOAuthButton>
        );

  storiesOf('UI|Forms/LogInForm', module)
    .addDecorator(routerAuthentication())
    .add('default', () =>
         <Container>
           <LogInForm/>
         </Container>
        )
    .add('in modal', () =>
         <LogInModal
           trigger={<Link to="/">Login</Link>}
         />
        );

  storiesOf('UI|Forms/RegistrationForm', module)
    .addDecorator(routerAuthentication())
    .add('default', () =>
         <Container>
           <RegistrationForm />
         </Container>
        );
}
