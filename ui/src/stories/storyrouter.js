import StoryRouter from 'storybook-react-router';
import { linkTo } from '@storybook/addon-links';

export function getStoryRouterWithLocation(links, path) {
  const opts = path ? {
    initialEntries: [path]
  } : null;
  return StoryRouter(links, opts);
}

export function getStoryRouterCreateAgenda(path) {
  const links = {
    '/create-agenda': linkTo('Screens|Create Agenda', '1: New'),
    '/create-agenda/1/add-participants': linkTo('Screens|Create Agenda', '2: Add Participants'),
    '/create-agenda/1/add-items': linkTo('Screens|Create Agenda', '3: Add Items'),
    '/create-agenda/1/finalize': linkTo('Screens|Create Agenda', '4: Finalize'),
    '/create-agenda/1/results': linkTo('Screens|Create Agenda', '5: See Results')
  };
  return getStoryRouterWithLocation(links, path);
}

export function getStoryRouterVote(path) {
  const links = {
    '/rank-submit': linkTo('Screens|Vote', 'Confirm'),
    '/rank-abstain': linkTo('Screens|Vote', 'Abstain'),
  };
  return getStoryRouterWithLocation(links, path);
}

