import apolloStorybookDecorator from 'apollo-storybook-react';
import typeDefs from '../../../schema/schema.js';

export default function withGraphQL(mocks){
  return apolloStorybookDecorator({typeDefs, mocks});
}

