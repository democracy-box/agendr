import gql from 'graphql-tag';

export const AllAgendas = gql`
query allAgendas {
  allAgendas {
    id
    title
    description
    length
    status
  }
}
`

export const AllAgendaItems = gql`
query allAgendaItems {
  allAgendaItems {
    id
    title
    description
    length
  }
}
`

export const Agenda = gql`
query Agenda($id: ID!) {
  agenda(id: $id) {
    id
    title
    description
    length
    status
    participants {
      id
      email
    }
    items {
      id
      title
      description
      length
    }
  }
}
`

export const AgendaWithResults = gql`
query Agenda($id: ID!) {
  agenda(id: $id) {
    id
    title
    description
    length
    status
    participants {
      id
      email
    }
    items {
      id
      title
      description
      length
      result {
        # rank
        mean
        median
        approvalCount
        distribution
      }
    }
  }
}
`


export const AgendaItems = gql`
  query agendaItems($agendaId: ID!) {
    agendaItems(agendaId: $agendaId) {
      id
      title
      description
      length
    }
  }
`

export const AgendaParticipant = gql`
query agendaParticipant($id: ID!) {
  agendaParticipant(id: $id) {
    id
    email
    startDate
    submitDate
    agenda {
      id
      meetingLength: length
      title
      description
    }
    agendaItemRankings {
      id
      rank
      approval
      agendaItem {
        id
        title
        description
        length
      }
    }
  }
}
`

export const AgendaParticipants = gql`
query agendaParticipants($agendaId: ID!) {
  agenda(id: $agendaId) {
    participants {
      id
      email
    }
  }
}
`

export const CreateAgenda = gql`
mutation CreateAgenda($title: String!, $description: String!, $length: Int, $facilitator: ID) {
  createAgenda(title: $title, description: $description, length: $length, facilitator: $facilitator) {
    id
    title
    description
    length
  }
}
`

export const AddAgendaItem = gql`
mutation AddAgendaItem($agendaId: ID!, $title: String!, $description: String!, $length: Int!) {
  addAgendaItem(agendaId: $agendaId, title: $title, description: $description, length: $length) {
    id
    title
    description
    length
  }
}
`

export const DeleteAgendaItem = gql`
mutation DeleteAgendaItem($agendaItemId: ID!) {
  deleteAgendaItem(agendaItemId: $agendaItemId) {
    id
    items {
      id
      title
      length
      description
    }
  }
}
`
export const AddAgendaParticipant = gql`
mutation addAgendaParticipant($agendaId: ID!, $email: String!) {
  addAgendaParticipant(agendaId: $agendaId, email: $email) {
    id
    # TODO this should be a user ref, not an email string.
    email
    # TODO refactor so this is a full nested  object, as below
    # agendaId
    # agenda {
    #   id
    # }
    # user {
    #   id
    #   email
    # }
  }
}
`

export const GetLocalRankedAgendaItems = gql`
query GetLocalRankedAgendaItems($agendaId: ID!, $participantId: ID!) {
  rankedAgendaItems(agendaId: $agendaId, participantId: $participantId) @client {
    id
    rank
    approval
    agendaItemId
  }
}
`

// # NOTE that approval is not set directly. Rank is specified and the server
// # determines approval based on meeting length.
export const SetLocalParticipantAgendaItemRank = gql`
mutation setLocalRankedAgendaItemRank($agendaId: ID!, $participantId: ID!, $agendaItemId: ID!, $rank: Int!) {
  setParticipantAgendaItemRank(agendaId: $agendaId, participantId: $participantId, agendaItemId: $agendaItemId, rank: $rank) @client {
    id
    participantId
    agendaId
    agendaItemId
    rank
    approval
  }
}
`

export const GetRankedAgendaItems = gql`
query GetRankedAgendaItems($agendaId: ID!, $participantId: ID!) {
  rankedAgendaItems(agendaId: $agendaId, participantId: $participantId) {
    id
    rank
    approval
    agendaItemId
  }
}
`

export const SetParticipantAgendaItemRank = gql`
mutation setParticipantAgendaItemRank($participantId: ID!, $agendaItemId: ID!, $rank: Int!) {
  setRankedAgendaItemRank(participantId: $participantId, agendaItemId: $agendaItemId, rank: $rank) {
    id
    rank
    approval
    agendaItem
  }
}
`

export const SetAgendaItemRanking = gql`
mutation setAgendaItemRanking($id: ID!, $rank: Int!) {
  setAgendaItemRanking(id: $id, rank: $rank) {
    id
    rank
    approval
    agendaItem {
      id
      title
      description
      length
    }
  }
}
`

export const SetAgendaParticipantStatus = gql`
mutation setAgendaParticipantStatus($id: ID!, $status: BallotStatus!) {
  setAgendaParticipantStatus(id: $id, status: $status) {
    id
    status
  }
}
`

export const SetAgendaStatus = gql`
mutation setAgendaStatus($agendaId: ID!, $status: String!) {
  setAgendaStatus(agendaId: $agendaId, status: $status) {
    id
    status
  }
}
`

export const SetAgendaParticipantStartDate = gql`
mutation setAgendaParticipantStartDate($id:ID!, $startDate:Int!) {
  setAgendaParticipantStartDate(id: $id, startDate: $startDate) {
    id
    startDate
  }
}`

export const SetAgendaParticipantSubmitDate = gql`
mutation setAgendaParticipantSubmitDate($id:ID!, $submitDate:Int!) {
  setAgendaParticipantSubmitDate(id: $id, submitDate: $submitDate) {
    id
    submitDate
  }
}`

export const AuthenticationURI = gql`
query authenticationURI($input: AuthenticationURIInput!) {
  authenticationURI(input: $input) {
    service
    loginURI
    redirectURI
  }
}`

export const UserForJWT = gql`
query userForJWT($jwt: String!) {
  userForJWT(jwt: $jwt) {
    User
  }
}`
