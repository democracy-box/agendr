/* eslint-disable */
// require('module-alias/register');
// import moduleAlias from 'module-alias';
// moduleAlias.addAliases({
//   "@root": ".",
//   "@components": __dirname + "src/components/",
//   "@containers": __dirname + "src/containers/",
//   "@bindings": __dirname + "src/bindings/",
//   "@common": __dirname + "src/common/"
// })
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { graphql } from 'react-apollo';
import * as schema from './schema';
// import Claims from './components/Claims'
// import Users from './components/Users'
// import User from './components/User'
import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container, Header, List } from 'semantic-ui-react';
import AgendaCreate from 'containers/AgendaCreate'
import ParticipantAgendaVote from 'containers/ParticipantAgendaVote'
import OAuthButton from 'bindings/OAuthButton'
import AllAgendas from 'bindings/AllAgendas'
/* eslist-enable */

class App extends Component {
  render() {
    return (
      <Router>
        <Container as="div" className="App">
        <Header className="App-title" as="h1" attached="top">welcome to agendr</Header>
          <Segment as="header" attached>
            <List horizontal>
              <List.Item>
                <List.Content>
                  <Link to="/">Create New Agenda</Link>
                  <Link to="/all">View Previous Agendas</Link>
                </List.Content>
              </List.Item>
            </List>
          </Segment>
          <Segment>
            <Route path="/" component={AgendaCreate} />
            <Route path="/all" component={AllAgendas} />
            <Route path="/vote" component={ParticipantAgendaVote} />
          </Segment>
        </Container>
      </Router>
    );
  }
}

export default App;
