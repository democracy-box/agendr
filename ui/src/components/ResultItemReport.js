import React, { Component } from 'react';
import { Header, Container, Table, Item, Label, Segment } from 'semantic-ui-react';
import AgendaItem from 'components/AgendaItem';
import './Agenda.css';

export default class ResultItemReport extends Component {
  render() {
    const { items, meetingLength, ListItemComponent } = this.props;
    const count = items.length;
    let itemsLength = 0;
    return (
      <Table structured compact size="small" unstackable striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell rowspan="2">Item</Table.HeaderCell>
            <Table.HeaderCell rowspan="2">Mean</Table.HeaderCell>
            <Table.HeaderCell rowspan="2">Median</Table.HeaderCell>
            <Table.HeaderCell colspan={count}>Distribution</Table.HeaderCell>
          </Table.Row>
          <Table.Row>
            {[...Array(count)].map((_, i) => <Table.HeaderCell>#{i+1}</Table.HeaderCell>)}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {items && items.map((item, index) => {
            itemsLength += item.length;
            const meetingFit = item.result && typeof item.result.approval !== "undefined" ? item.result.approval : itemsLength <= meetingLength;
            return (
          <Table.Row>
            <Table.Cell>{item.title}</Table.Cell>
            <Table.Cell>{item.result.mean+1}</Table.Cell>
            <Table.Cell>{item.result.median+1}</Table.Cell>
            {[...Array(count)].map((_, i) => <Table.Cell>{item.result.distribution[i] ? item.result.distribution[i] : "0"}</Table.Cell>)}
          </Table.Row>);
            })}
        </Table.Body>
      </Table>
    );
  }
}
