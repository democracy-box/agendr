import React, { Component } from 'react';
import { Header, Container, List, ListItem, Item, Label, Grid, Button, Icon } from 'semantic-ui-react';

function minutesAsTime(minutes) {
  // return moment().minute()
  const hours = Math.floor(minutes/60);
  const hourString = hours.toString();
  const remainder = (minutes % 60);
  const remainderString = remainder < 10 ? "0" + remainder.toString() : remainder.toString(); 
  return `${hourString}:${remainderString}`;
}

export default class AgendaItem extends Component {
  render() {
    const { agendaItemId, title, description, links, length, result, meetingFit, pos, count, editable, as, deleteAgendaItem} = this.props;
    let classes = meetingFit ? "agendr-agenda-item--meeting-fit" : "agendr-agenda-item--no-meeting-fit";
    const posInt = Number.isInteger(pos) ? pos : parseInt(pos, 10);
    const ItemComponent = as || Item;
    const handleClickDeleteButton = () => {
      deleteAgendaItem(agendaItemId);
    };
    return (
      <ItemComponent key={agendaItemId}>
        <Item.Content relaxed>
          <Item.Header as="h3">
            {title}
          </Item.Header>
          <Item.Description>
            {description}
          </Item.Description>
          <Item.Extra>
            { editable &&
              <Button basic compact color="red" size='mini' icon="minus" content="Delete" onClick={handleClickDeleteButton}></Button>
            }
          </Item.Extra>
          <Label attached="top right">
            {minutesAsTime(length)}
          </Label>
        </Item.Content>
      </ItemComponent>
    );
  } 
}

