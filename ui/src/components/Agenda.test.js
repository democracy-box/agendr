import React from 'react';
import ReactDOM from 'react-dom';
import SortableAgendaItemList from './SortableAgendaItemList';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SortableAgendaItemList />, div);
  ReactDOM.unmountComponentAtNode(div);
});
