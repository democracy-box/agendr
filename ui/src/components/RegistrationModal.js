import React, { Component } from 'react';
import { Modal, Header, Button, Input, Form, Icon } from 'semantic-ui-react';

export default ({values, handleChange, trigger}) =>
  <Modal trigger={trigger || <Button>Login</Button>} closeIcon centered={false}>
    <Header content="Register"/>
    <Modal.Content>
    </Modal.Content>
    <Modal.Actions>
      <Button
        id='agendr-register-submit'
        icon
        primary
      >
        Create Account <Icon name="arrow right" />
      </Button>
    </Modal.Actions>
  </Modal>
