import React, { Component } from 'react';
import { Header, Container, List, ListItem, Item, Label, Segment } from 'semantic-ui-react';
import AgendaItemList from 'components/AgendaItemList.js';
import BoundAgendaItemList from 'bindings/AgendaItemList.js';
import BoundAgendaParticipantList from 'bindings/AgendaParticipantList.js';
import './Agenda.css';

function minutesAsTime(minutes) {
  const minutesString = (minutes % 60).toString();
  return Math.floor(minutes/60).toString() + ":" + ("00" + minutesString).substr(minutesString.length);
}

export default class Agenda extends Component {
  render() {
    const { title, description, length, agendaId, items, showItems, showParticipants, showParticipantVoteLinks } = this.props;
    return (
      <Container className="Agenda">
        <Header as='h2' textAlign='center'>
          <Header.Content>
            {title}
            <Header.Subheader>
              <div className="agendr-agenda-description">
                {description}
              </div>
              <div className="agendr-agenda-length">
                Duration:  {minutesAsTime(length)}
              </div>
            </Header.Subheader>
          </Header.Content>
        </Header>
        {showParticipants && (agendaId && (
          <Segment>
            <Header as="h3">
              Participants
            </Header>
            <BoundAgendaParticipantList agendaId={agendaId} showVoteLinks={showParticipantVoteLinks} />
          </Segment>
        ))}
        {showItems && (
          <Segment>
            <Header as="h3">
              Items
            </Header>
            <Container align="left">
              {agendaId ? (
                <BoundAgendaItemList meetingLength={length} agendaId={agendaId} />
              ) : (
                <AgendaItemList meetingLength={length} items={items} />
              )}
            </Container>
          </Segment>
        )}
      </Container>
    );
  }
}
