import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Button, Form, Icon } from 'semantic-ui-react';

const OAuthButton = ({loginURI, service, children, fluid=true, location}) => {
  let icon, color, defaultText;
  switch (service) {
  case 'SLACK':
    icon = 'slack';
    color = 'green';
    defaultText = 'Log In with Slack';
    break;
  case 'GOOGLE':
    icon = 'google';
    color = 'blue';
    defaultText = "Log In with Google";
    break;
  case 'GITHUB':
    color = icon = 'github';
    defaultText = "Log In with GitHub";
    break;
  case 'FACEBOOK':
    color = icon = 'facebook';
    defaultText = "Log In with Facebook";
    break;
  case 'BITBUCKET':
    icon = 'bitbucket';
    defaultText = "Log In with BitBucket";
    break;
  }
  return <Button icon={typeof icon !== "undefined"} labelPosition='left' color={color} href={loginURI} fluid={fluid}>
           {(typeof icon !== "undefined") && (<Icon name={icon}/>)}
           {children || defaultText}
         </Button>;
}

export default withRouter(OAuthButton);
