import React, { Component } from 'react';
import { withRouter, Link } from "react-router-dom";
import { Header, Container, List, ListItem, Item, Label, Segment } from 'semantic-ui-react';
import './Agenda.css';

function AgendaParticipant({participant, showVoteLink}) {
  return <Item.Content>
           <Item.Header>
             {participant.email} 
           </Item.Header>
           { 
             showVoteLink ?
               (<Item.Description><Link to={'/vote/' + participant.id}>Vote</Link></Item.Description>) : ''
           }
         </Item.Content>;
}

export default function AgendaParticipantList({participants, showVoteLinks}) {
  return <List>
           {participants && participants.map((participant, idx) => (
             <List.Item key={idx}>
               <AgendaParticipant participant={participant} showVoteLink={showVoteLinks} />
             </List.Item>
           ))}
         </List>;
}

