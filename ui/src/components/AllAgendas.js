import React, { Component } from 'react';
import { Header, Container, Segment, Dropdown, Form, Table } from 'semantic-ui-react';

export default class AllAgendas extends Component {
  render() {
    const { agendas, match } = this.props;
    const basePath = match && match.path && match.path !== "/" ? match.path : "";

    return (
      <div>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Title</Table.HeaderCell>
              <Table.HeaderCell>Description</Table.HeaderCell>
              <Table.HeaderCell>Length</Table.HeaderCell>
              <Table.HeaderCell>Status</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            { agendas && agendas.map((agenda) => {
              return <Table.Row key={agenda.id}>
                       <Table.Cell>
                       <a href={`/${agenda.id}/see-results`}>{agenda.title}</a>
                       </Table.Cell>
                       <Table.Cell>
                       {agenda.description}
                       </Table.Cell>
                       <Table.Cell>
                       {agenda.length}
                       </Table.Cell>
                       <Table.Cell>
                       {agenda.status}
                       </Table.Cell>
                     </Table.Row>;
            })}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

