import React, { Component } from 'react';
import { Header, Container, List, Label, Segment } from 'semantic-ui-react';
import AgendaItem from 'components/AgendaItem';
import './Agenda.css';

export default class AgendaItemList extends Component {
  render() {
    const { items, meetingLength, ListItemComponent, editable, itemAs } = this.props;
    let itemsLength = 0;
    const AgendaItemComponent = itemAs || AgendaItem;
    return (
      <List relaxed ordered size="large">
        {items && items.map((item, index) => {
          itemsLength += item.length;
          const meetingFit = itemsLength <= meetingLength;
          return (
            <AgendaItemComponent
              key={item.id}
              agendaItemId={item.id}
              index={index}
              title={item.title}
              meetingFit={meetingFit}
              length={item.length}
              description={item.description}
              links={item.links}
              pos={index+1}
              as={List.Item}
              editable={editable}
            />
          );
        })}
      </List>
    );
  }
}
