import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Step, Icon } from 'semantic-ui-react'

class AgendaSteps extends Component {
  render() {
    let { activeStep, match, agenda } = this.props;
    const agendaId = match && match.params && match.params.agendaId || null;
    console.log(agenda);
    const started = typeof agenda === "object" && agenda.status !== 'open';
    activeStep = activeStep ? parseInt(activeStep, 10) : 1; 
    return (
      <Step.Group attached>
        <Step
          active={activeStep==1}
        >
          <Step.Content>
            <Step.Title as={Link} to="/">Create Agenda</Step.Title>
          </Step.Content>
        </Step>

        <Step
          active={activeStep==2}
        >
          <Step.Content>
            <Step.Title as={Link} to={`/${agendaId}/add-participants`}>Add Participants</Step.Title>
          </Step.Content>
        </Step>

        <Step
          active={activeStep==3}
        >
          <Step.Content>
            <Step.Title as={Link} to={`/${agendaId}/add-items`}>Add Items</Step.Title>
          </Step.Content>
        </Step>

        <Step
          active={activeStep==4}
        >
          <Step.Content>
            <Step.Title as={Link} to={`/${agendaId}/start-vote`}>Start Voting</Step.Title>
          </Step.Content>
        </Step>

        <Step
          active={activeStep==5}
          disabled={!started}
        >
          <Step.Content>
            <Step.Title as={Link} to={`/${agendaId}/see-results`}>Get Results</Step.Title>
          </Step.Content>
        </Step>
      </Step.Group>
    );
  }
}

export default withRouter(AgendaSteps);
