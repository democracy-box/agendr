import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Container, Modal, Button, Input, Form, Icon, Divider } from 'semantic-ui-react';

const DefaultRegisterLink = () => <Link to="/register">Register here.</Link>;

export default ({showButtons=true,
                 RegistrationLink=DefaultRegisterLink
                }) =>
  <Container fluid>
    Enter your username and password to log in.
    { RegistrationLink &&
      <span>
        &nbsp;
        Need to create an account?
        &nbsp;
        <RegistrationLink />
      </span>
    }
    <Divider hidden />
    <Form>
      <Form.Field>
        <Input
          id='agendr-login-email'
          label='Email'
          name="email"
/* value={values && values.email || ''} */
/* onChange={handleChange} */
        />
      </Form.Field>
      <Form.Field>
        <Input
          type='password'
          id='agendr-login-password'
          label='Password'
          name="password"
/* value={values && values.password || ''} */
/* onChange={handleChange} */
        />
      </Form.Field>
      {showButtons &&
       <Button
         id='agendr-login-submit'
         icon
         primary
         floated="right"
       >
         Log In<Icon name="arrow right" />
       </Button>
      }
    </Form>
  </Container>


