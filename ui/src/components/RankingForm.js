import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';

import { Button, Input, TextArea, Form, SegmentGroup, Segment, Container } from 'semantic-ui-react';
import Agenda from '../components/Agenda';

class RankingForm extends Component {
  render() {
    const { agendaItems } = this.props;
    return(
      <SegmentGroup>
        <Segment textAlign='center'>
          <h2>Agenda Ranking Form</h2>
          <Container text textAlign='left'>
            <p>Use the form below to rank the proposed agenda item in the order you think they should be presented. Any items that appear grayed out would exceed the proposed meeting time. Once complete, submit your ranking with any additional comments. Your ranking will then be averaged with others to arrive at a collective arrangement based on equal input from all participants.</p>
          </Container>
        </Segment>
        <Segment>
          <Agenda
            sortable
            title="CoLab Coop AgendR Meeting"
            meetingLength="45"
            description="A really important meeting to discuss the process for deciding what we should meet on at future meetings."
            items={agendaItems} / >
        </Segment>
        <Segment>
          <Form size="large">
          <Form.Field
            inline 
            id='agendr-ranking-comment-input'
            control={TextArea}
            label='Comments'
            placeholder='Meeting objective, summary, etc.'
          />
          <Form.Field>
            <Button
              id='agendr-agenda-submit'
              content='Submit Ranking'
              primary
              attached="top"
              as={Link}
              to="/rank-submit"
              />
            <Button
              id='agendr-agenda-abstain'
              content='Abstain'
              compact
              attached="bottom"
              as={Link}
              to="/rank-abstain"
              />
          </Form.Field>
          </Form>
        </Segment>
      </SegmentGroup>
    );
  }
}

export default withRouter(RankingForm);
