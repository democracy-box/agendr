import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Header, Container, Form, Input, Button, Icon, List, ListItem, Item, Label, Segment, Dimmer } from 'semantic-ui-react';

class AgendaParticipantsForm extends Component {
  render () {
    const { agendaId, agenda, values, addAgendaParticipant, participants, handleSubmit, handleChange } = this.props;
    const editable = typeof agenda === "object" && agenda.status === 'open';
    return (
      <Container fluid>
        <Dimmer active={!editable}>
          <Container>Voting on this agenda has been initiated, participants can no longer be added.</Container>
        </Dimmer>
        <Form onSubmit={handleSubmit}>
          <Form.Field>
            <Input
              id='agendr-agenda-participant-email-input'
              name="email"
              placeholder="participant@example.email"
              onChange={handleChange}
              value={values.email || ''}
              label="Email"
            />
          </Form.Field>
          <Form.Field>
            <Button icon fluid><Icon name="plus"/> Add Participant</Button>
          </Form.Field>
        </Form>
      </Container>
    );
  } 
}

export default AgendaParticipantsForm;
