import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Button, Input, TextArea, Form, Header, Icon, Loader, Dimmer} from 'semantic-ui-react';
const Group = Form.Group;
const Field = Form.Field;

class ParticipantSubmitVoteForm extends Component {
  render() {
    const { agendaId, submitDate, values, RankingFormsetAgendaParticipantSubmitDate, handleSubmit, handleChange, handleBlur, match } = this.props;
    return(
        <Form
          onSubmit={handleSubmit}
          >
        <Field>
        { (() => {
          if (submitDate === undefined) {
            return <Dimmer active>
              <Loader />
            </Dimmer>
          }
          else if (submitDate === null) {
            return <Button icon fluid color="red" type="submit">
              Submit Final Vote
              <Icon name="right arrow"/>
            </Button>
          }
          else {
            return <Button fluid disabled>
              Vote Submitted
            </Button>
          }
        }
          )()
        }
        </Field>
        </Form>
    );
  }
}

export default withRouter(ParticipantSubmitVoteForm);
