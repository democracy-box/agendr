import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import OAuthButton from 'bindings/OAuthButton';
import { Container, Divider } from 'semantic-ui-react';

export default ({redirectURI, services}) => {
  return <Container>
           {
             services.map((service, i) =>
                          [
                            i > 0 && <Divider hidden/>,
                            <OAuthButton service={service} {...{redirectURI}}></OAuthButton>
                          ]
                         )
           }
         </Container>
}
