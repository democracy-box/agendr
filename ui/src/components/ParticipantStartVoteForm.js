import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Button, Input, TextArea, Form, Header, Icon, Loader, Dimmer} from 'semantic-ui-react';
const Group = Form.Group;
const Field = Form.Field;

class ParticipantStartVoteForm extends Component {
  render() {
    const { agendaId, startDate, values, RankingFormsetAgendaParticipantStartDate, handleSubmit, handleChange, handleBlur, match } = this.props;
    return(
        <Form
          onSubmit={handleSubmit}
          >
          <Field>
            { (() => {
              if (startDate === undefined) {
                return <Dimmer active>
                         <Loader />
                       </Dimmer>
              }
              else if (startDate === null) {
                return <Button icon fluid color="red" type="submit">
                 Start Voting
                 <Icon name="right arrow"/>
               </Button>
              }
              else {
                return <Button icon fluid primary
                       as={Link}
                       to={`${match.url}/rank`}
               >
                 Resume Started
                 <Icon name="right arrow"/>
               </Button>
              }
            })()
            }
          </Field>
        </Form>
    );
  }
}

export default withRouter(ParticipantStartVoteForm);
