import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Container, Button, Input, TextArea, Form, Icon } from 'semantic-ui-react';

class AgendaForm extends Component {
  render() {
    const { values, handleSubmit, handleChange, handleBlur } = this.props;
    return(
        <Form
          size="large"
          onSubmit={handleSubmit}
        >
        <Form.Group inline>
        <Form.Field inline width="10">
          <Input
            id='agendr-agenda-title-input'
            label='Title'
            placeholder='Meeting Title'
            width='12'
            name="title"
            value={values && values.title || ''}
            onChange={handleChange}
          />
        </Form.Field>
        <Form.Field inline width="6">
        <Input
          id='agendr-agenda-length-input'
          label='Meeting Length'
          placeholder='Minutes'
          width='4'
          name="length"
          value={values && values.length || ''}
          onChange={handleChange}
        />
        </Form.Field>
        </Form.Group>
        <Form.Field inline>
        <TextArea
          id='agendr-agenda-description-input'
          label='Summary'
          placeholder='Meeting objective, summary, etc.'
          name="description"
          value={values && values.description || ''}
          onChange={handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Button
            id='agendr-submit--agenda-create-new'
            icon
            primary
            fluid
            /* as={Link} */
            /* to="/create-agenda/add-participants" */
            >
            Next: Add Participants <Icon name="arrow right" />
          </Button>
        </Form.Field>
        </Form>
    );
  }
}

export default withRouter(AgendaForm);
