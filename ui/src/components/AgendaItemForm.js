import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Container, Button, Input, TextArea, Form, Header, Icon, Dimmer } from 'semantic-ui-react';
const Group = Form.Group;
const Field = Form.Field;

class AgendaItemForm extends Component {
  render() {
    const { agendaId, values, agenda, addAgendaItem, handleSubmit, handleChange, handleBlur } = this.props;
    const editable = typeof agenda === "object" && agenda.status === 'open';
    return(
      <Container fluid>
        <Dimmer active={!editable}>
          <Container>Voting on this agenda has been initiated, items can no longer be added.</Container>
        </Dimmer>
        <Form
          onSubmit={handleSubmit}
        >
          <Header>Add Agenda Item</Header>
          <Group inline>
            <Field width="12" inline>
              <Input
                id='agendr-agenda-item-title-input'
                name="title"
                label='Title'
                value={values.title || ''}
                placeholder='Item Title'
                onChange={handleChange}
                disabled={!editable}
              />
            </Field>
            <Field width="4" inline>
              <Input
                id='agendr-agenda-item-length-input'
                name='length'
                value={values.length || ''}
                label='Length'
                placeholder='Minutes'
                onChange={handleChange}
                disabled={!editable}
              />
            </Field>
          </Group>
          <Field>
            <TextArea
              id='agendr-agenda-item-description-input'
              name='description'
              value={values.description || ''}
              label='Description'
              placeholder='Item Description'
              onChange={handleChange}
              disabled={!editable}
            />
          </Field>
          <Field>
            <Button icon fluid type="submit" disabled={!editable}><Icon name="plus"/> Add Item</Button>
          </Field>
        </Form>
      </Container>
    );
  }
}

export default AgendaItemForm;
