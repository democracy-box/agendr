import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Button, Input, TextArea, Form, Header, Icon } from 'semantic-ui-react';
const Group = Form.Group;
const Field = Form.Field;

class AgendaStartVoteForm extends Component {
  render() {
    const { agendaId, agendaStatus, values, setAgendaStatus, handleSubmit, handleChange, handleBlur } = this.props;
    return(
        <Form
          onSubmit={handleSubmit}
          >
        <Field>
        { agendaStatus === "open" ? (
            <Button icon fluid color="red" type="submit"><Icon name="lock open"/> Finalize Agenda and Open Voting</Button>
        ) : (
            <Button icon fluid disabled type="submit"><Icon name="lock"/> Closed for Voting</Button>
        )}
          
        </Field>
        </Form>
    );
  }
}

export default AgendaStartVoteForm;
