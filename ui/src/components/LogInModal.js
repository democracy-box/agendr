import React from 'react';
import { Segment, Modal, Button, Input, Form, Icon } from 'semantic-ui-react';
import LogInForm from 'components/LogInForm';

export default ({values, handleChange, trigger}) =>
  <Modal trigger={trigger || <Button>Login</Button>} closeIcon centered={false}>
    <Modal.Header content="Log In"/>
    <Modal.Content>
      <LogInForm showButtons={false} />
    </Modal.Content>
    <Modal.Actions>
      <Button
        id='agendr-login-submit'
        icon
        primary
      >
        Log In<Icon name="arrow right" />
      </Button>
    </Modal.Actions>
  </Modal>;
