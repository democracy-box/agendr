import React, { Component } from 'react';
import { Header, Container, Segment, Dropdown, Form, Table } from 'semantic-ui-react';
import AgendaItem from 'components/AgendaItem';
import './ItemRankTable.css';

export default class ItemRankTable extends Component {
  render() {
    const { items, meetingLength, handleChange, handleDropdownChange, handleSubmit } = this.props;
    const orderOptions = items && items.map((item, index) => {
      return {
        value: index,
        key: index,
        text: index+1
      };
    });
    let itemLengthRunningTotal = 0;
    return <Form onSubmit={handleSubmit}>
             <Table>
             <Table.Header>
               <Table.Row>
                 <Table.HeaderCell>Order</Table.HeaderCell>
                 <Table.HeaderCell>Item</Table.HeaderCell>
                 <Table.HeaderCell>Length</Table.HeaderCell>
               </Table.Row>
             </Table.Header>
             <Table.Body>
               { items && items.map((item, index) => {
                 itemLengthRunningTotal += item.length;
                 return <Table.Row key={item.id} className={item.approval ? 'agendr-item-rank--under-time' : 'agendr-item-rank--over-time'}>
                          <Table.Cell>
                            <Form.Dropdown
                              options={orderOptions}
                              value={item.rank}
                              name={item.id}
                              onChange={handleDropdownChange}/>
                          </Table.Cell>
                          <Table.Cell>
                            <Header as="strong">
                              {item.agendaItem.title}
                            </Header>
                            <Container as="p" fluid>
                              {item.agendaItem.description}
                            </Container>
                          </Table.Cell>
                          <Table.Cell>
                            <Container as="span" fluid>
                              {item.agendaItem.length}
                            </Container>
                          </Table.Cell>
                        </Table.Row>;
               })}
             </Table.Body>
           </Table>
           </Form>
    ;
  }
}
