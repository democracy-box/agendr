import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Container, Divider, Modal, Button, Input, Form, Icon } from 'semantic-ui-react';

const DefaultLogInLink = () => <Link to="/login">Log in here.</Link>

export default ({showButtons=true,
                 LogInLink=DefaultLogInLink
                }) =>
      <Container fluid>
        Enter your email and password to register.
        { LogInLink &&
          <span>
            &nbsp;
            Need to create an account?
            &nbsp;
            <LogInLink />
          </span>
        }
        <Divider hidden />
        <Form>
          <Form.Field>
            <Input
              id='agendr-register-email'
              label='Email'
              name="email"
/* value={values && values.email || ''} */
/* onChange={handleChange} */
            />
          </Form.Field>
          <Form.Field>
            <Input
              type='password'
              id='agendr-register-password'
              label='Password'
              name="password"
/* value={values && values.password || ''} */
/* onChange={handleChange} */
            />
          </Form.Field>
          <Form.Field>
            <Input
              type='password'
              id='agendr-register-password'
              label='Confirm Password'
              name="passwordConfirmation"
/* value={values && values.password || ''} */
/* onChange={handleChange} */
            />
          </Form.Field>
          {showButtons &&
           <Form.Field>
             <Button
               id='agendr-registration-submit'
               icon
               primary
               floated="right"
             >
               Register &nbsp; <Icon name="arrow right" />
             </Button>
           </Form.Field>
          }
        </Form>
      </Container>
