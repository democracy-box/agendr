/**
 * Helper functions for working with routing and paths.
 */

export function basePathFromMatch(match) {
  const basePath = match && match.url && match.url !== "/" ? match.url : "";
  return basePath;
}
