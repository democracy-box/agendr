import React, { Component, Fragment } from 'react';
import { Formik, withFormik } from 'formik';
import Yup from 'yup';
import { Form as SForm, Button as SButton, Input as SInput } from 'semantic-ui-react';

// TODO fix this, currently breaks the handlers.
export const formComponents = ({handleChange, handleBlur}) => {
  return {
    Form: SForm,
    Button: SButton,
    Field: SForm.Field,
    Group: SForm.Group,
    Input: (props) => <SInput
                         onChange={handleChange}
                         onBlur={handleBlur}
                         {...props}
                      />,
    TextArea: (props) => <SForm.TextArea
                        onChange={handleChange}
                        onBlur={handleBlur}
                        {...props}
                      />
  };
};

export const withSemanticUIFormik = props=> WrappedComponent=>{

  return withFormik(props)(class extends Component{
    handleBlur = (e, data) => {
      if(data && data.name){
        this.props.setFieldValue(data.name,data.value);
        this.props.setFieldTouched(data.name);
      }
    }

    handleChange = (e,data) =>{
      if (data && data.name) {
        this.props.setFieldValue(data.name,data.value);
      }
    }
    
    handleDropdownChange = (e, data) => {
      if (typeof props.handleDropdownChange === 'function') {
        props.handleDropdownChange.call(this, e, data);
      } else {
        data || data.name || this.props.setFieldValue(data.name, data.value);
      }
    };

    render(){
      return <WrappedComponent {...this.props}
                               handleBlur={this.props.handleBlur ? (...args) => this.props.handleBlur.apply(this, args) : this.handleBlur}
                               handleDropdownChange={this.handleDropdownChange}
                               handleChange={this.props.handleChange ? (...args) => {
                                 this.props.handleChange.apply(this, args);
                               } : this.handleChange}
             />
    }
  })
}
