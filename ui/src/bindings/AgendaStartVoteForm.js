import React from 'react';
import { compose, graphql } from 'react-apollo';
import { withSemanticUIFormik } from 'common/form';
import * as schema from '../schema';

import AgendaStartVoteForm from 'components/AgendaStartVoteForm';

const handleSubmit = (payload, { props, setSubmitting, setErrors, resetForm }) => {
  const { title, length, description } = payload;
  const { agendaId } = props;
  const status = 'voting';
  props.setAgendaStatus({ agendaId, status })
    .then((response) => {
      // props.data.refetch();
      // resetForm();
      // props.history.push('/')
    }).catch((e) => {
      alert('error submitting');
      // setSubmitting(false)
      // setErrors({ email: ' ', password: ' ', form: errors })
    });
};

const AgendaStartVoteFormWithFormik = withSemanticUIFormik({
  // mapPropsToValues: (vals) => {
  //   return ({
  //     title: null, length: null, description: null
  //   });
  // },
  displayName: "AgendaStartVoteForm",
  handleSubmit: handleSubmit
})(AgendaStartVoteForm);

const AgendaStartVoteFormWithGraphQL = compose(
  graphql(schema.Agenda, {
    options: ({agendaId}) => ({
      variables: {
        id: agendaId
      }
    }),
    props: ({data: {agenda = {}} = {agenda: {}}}) => ({ agendaStatus: agenda.status })
  }),
  graphql(schema.SetAgendaStatus, {
    props: ({ mutate }) => ({
      setAgendaStatus: (variables) =>
        mutate({variables,
                refetchQueries: [{
                  query: schema.Agenda,
                  variables: {id: variables.agendaId}
                }],
               })
    })
  })
)(AgendaStartVoteFormWithFormik);

export default (AgendaStartVoteFormWithGraphQL);
