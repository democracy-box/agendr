/**
 * @fileOverview
 * @name ItemRankTable.js
 * @author ethan@colab.coop
 * @license GPL + HumnaRights
 */

import React from 'react';
import { compose, graphql } from 'react-apollo';
import { withSemanticUIFormik } from 'common/form';
import * as schema from '../schema';

import ItemRankTable from 'components/ItemRankTable';

function handleDropdownChange(e, data) {
  const { name,value } = data;
  // const { setRankedAgendaItemRank } = this.props;
  this.props.setAgendaItemRanking({id: name, rank: value})
    .catch((e) => {
      alert('error setting value. please reload this page and start again.');
      // setSubmitting(false)
      // setErrors({ email: ' ', password: ' ', form: errors })
    });
};

const handleSubmit = (payload, { props, setSubmitting, setErrors, resetForm }) => {
  // const { title, length, description } = payload;
  // const { agendaId } = props;
  // props.setRankedAgendaItemRank({ agendaId, title, length, description })
  //   .then((response) => {
  //     // props.data.refetch();
  //     resetForm();
  //     // props.history.push('/')
  //   }).catch((e) => {
  //     alert('error submitting');
  //     console.log(e);
  //     // setSubmitting(false)
  //     // setErrors({ email: ' ', password: ' ', form: errors })
  //   });
};

const ItemRankTableWithFormik = withSemanticUIFormik({
  // mapPropsToValues: (vals) => {
  //   return ({
  //     title: null, length: null, description: null
  //   });
  // },
  displayName: "ItemRankTable",
  handleDropdownChange: handleDropdownChange,
  handleSubmit: handleSubmit
})(ItemRankTable);

const ItemRankTableWithGraphQL = compose(
  graphql(schema.AgendaParticipant, {
    options: ({participantId}) => ({
      variables: {
        id: participantId
      }
    }),
    // TODO: QUESTION Should these be renamed using the 'edges' convention for related nodes?
    props: ({data,
             data: {
               agendaParticipant,
               agendaParticipant: {
                 id,
                 email,
                 agenda,
                 agenda: {
                   meetingLength
                 } = {},
                 agendaItemRankings
               } = {}
             } = {}
            }) => {
      return ({
        id,
        email,
        agenda,
        meetingLength,
        items: agendaItemRankings
      });
    }
  }),
  graphql(schema.SetAgendaItemRanking, {
    props: ({mutate,
             ownProps: { participantId } = {},
             ...rest}) => ({
               setAgendaItemRanking: (variables) => {
                 return mutate({variables,
                                refetchQueries: [{
                                  query: schema.AgendaParticipant,
                                  variables: { id: participantId },
                                }],
                               }
                              );
               }
             })
  })

  // graphql(schema.AddAgendaItem, {
  //   props: ({ mutate }) => ({
  //     addAgendaItem: (variables) =>
  //       mutate({variables,
  //               // TODO Should we refresh or trust the cache?
  //               // refetchQueries: [{
  //               //   query: schema.AgendaItems,
  //               //   variables: {id: variables.agendaId}
  //               // }],
  //               update: (cache, {data: {addAgendaItem: newItem} = {}}) => {
  //                 const queryVars = {
  //                   agendaId: variables.agendaId
  //                 };
  //                 const data = cache.readQuery({
  //                   query: schema.AgendaItems,
  //                   variables: queryVars
  //                 });
  //                 data.agendaItems ? data.agendaItems.push(newItem) : data.agendaItems = [newItem];
  //                 cache.writeQuery({query: schema.AgendaItems, variables: queryVars, data: data});
  //               }})
  //   })
  // })
)(ItemRankTableWithFormik);

export default ItemRankTableWithGraphQL;
