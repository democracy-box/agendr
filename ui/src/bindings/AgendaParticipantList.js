import { compose, graphql } from 'react-apollo';
import * as schema from '../schema';

import AgendaParticipantList from 'components/AgendaParticipantList';

export default compose(
  graphql(schema.Agenda, {
    options: ({agendaId}) => ({
      variables: {
        id: agendaId
      }
    }),
    props: ({data}) => {
      const {agenda={}} = data;
      return ({
        participants: agenda.participants || []
      });
    }
  })
)(AgendaParticipantList);
