import React from 'react';
import { compose, graphql } from 'react-apollo';
import * as schema from '../schema';

import AgendaSteps from 'components/AgendaSteps';

const AgendaStepsWithGraphQL = compose(
  graphql(schema.Agenda, {
    options: ({agendaId}) => ({
      variables: {
        id: agendaId
      }
    }),
    props: ({data: {agenda} = {agenda: {}}}) => ({
      agenda  
    })
  }),
)(AgendaSteps);

export default (AgendaStepsWithGraphQL);
