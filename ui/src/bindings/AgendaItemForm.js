import React from 'react';
import { compose, graphql } from 'react-apollo';
import { withSemanticUIFormik } from 'common/form';
import * as schema from '../schema';

import AgendaItemForm from 'components/AgendaItemForm';

const handleSubmit = (payload, { props, setSubmitting, setErrors, resetForm }) => {
  const { title, length, description } = payload;
  const { agendaId } = props;
  props.addAgendaItem({ agendaId, title, length, description })
    .then((response) => {
      // props.data.refetch();
      resetForm();
      // props.history.push('/')
    }).catch((e) => {
      alert('error submitting');
      // setSubmitting(false)
      // setErrors({ email: ' ', password: ' ', form: errors })
    });
};

const AgendaItemFormWithFormik = withSemanticUIFormik({
  mapPropsToValues: (vals) => {
    return ({
      title: null, length: null, description: null
    });
  },
  displayName: "AgendaItemForm",
  handleSubmit: handleSubmit
})(AgendaItemForm);

const AgendaItemFormWithGraphQL = compose(
  graphql(schema.Agenda, {
    options: ({agendaId}) => ({
      variables: {
        id: agendaId
      }
    }),
    props: ({data: {agenda} = {agenda: {}}}) => ({agenda}) 
  }),
  graphql(schema.AddAgendaItem, {
    props: ({ mutate }) => ({
      addAgendaItem: (variables) =>
        mutate({variables,
                refetchQueries: [{
                  query: schema.Agenda,
                  variables: {id: variables.agendaId}
                }],
               })
    })
  })
)(AgendaItemFormWithFormik);

export default (AgendaItemFormWithGraphQL);
