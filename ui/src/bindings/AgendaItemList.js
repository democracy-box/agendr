import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import * as schema from '../schema';

import AgendaItemList from 'components/AgendaItemList';
import BoundAgendaItem from 'bindings/AgendaItem';

export default compose(
  graphql(schema.Agenda, {
    options: ({agendaId}) => ({
      variables: {
        id: agendaId
      }
    }),
    props: ({data}) => {
      const {agenda={}} = data;
      return ({
        items: agenda.items || [],
        editable: agenda.status === "open",
      });
    }
  }),
  (WrappedComponent) => (props) => <WrappedComponent {...props} itemAs={BoundAgendaItem} />
)(AgendaItemList);
