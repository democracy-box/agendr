import { compose, graphql } from 'react-apollo';
import * as schema from '../schema';

import RankingForm from 'components/RankingForm';

export default compose(
  graphql(schema.AgendaParticipant, {
    options: ({participantId}) => ({
      variables: {
        id: participantId
      }
    }),
    props: ({data: {agendaParticipant} = {agendaParticipant: {}}}) => agendaParticipant
  })
)(RankingForm);
