import { compose, graphql } from 'react-apollo';

import * as schema from '../schema';
import AgendaCreateAddItems from 'containers/AgendaCreate/AddItems';

export default compose(
  graphql(schema.AgendaItems, {
    options: (props) => ({
      variables: {
        id: props.agendaId
      }
    }),
    props: (data) => {
      const {data: {agendaItems}} = data;
      return ({
        items: agendaItems || []
      });
    }
  }),
  graphql(schema.AddAgendaItem, {
    props: ({ mutate }) => ({
      addAgendaItem: (variables) =>
        mutate({variables,
                update: (cache, {data: {addAgendaItem: newItem}}) => {
                  const queryVars = {
                    agendaId: variables.agendaId
                  };
                  const data = cache.readQuery({
                    query: schema.AgendaItems,
                    variables: queryVars
                  });
                  data.items ? data.items.unshift(newItem) : data.items = [newItem];
                  cache.writeQuery({query: schema.AgendaItems, variables: queryVars, data: data});
                }})
    })
  })
)(AgendaCreateAddItems);
