import { compose, graphql } from 'react-apollo';
import * as schema from '../schema';
import { Route, Redirect } from "react-router-dom";

import Agenda from 'components/Agenda';

export default compose(
  graphql(schema.Agenda, {
    options: ({agendaId}) => ({
      variables: {
        id: agendaId
      }
    }),
    props: ({data: {agenda} = {agenda: {}}}) => agenda
  })
)(Agenda);
