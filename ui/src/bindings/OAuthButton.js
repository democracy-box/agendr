import { compose, graphql } from 'react-apollo';
import * as schema from '../schema';

import OAuthButton from 'components/OAuthButton';

export default compose(
  graphql(schema.AuthenticationURI, {
    options: ({service, redirectURI}) => ({
      variables: {
        input: {
          service,
          redirectURI
        }
      }
    }),
    props: ({data}) => {
      console.log(data);
      const {authenticationURI={}} = data;
      return ({
        ...authenticationURI
      });
    }
  })
)(OAuthButton);
