import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose, graphql } from 'react-apollo';
import { withSemanticUIFormik } from 'common/form';
import * as schema from '../schema';
import { basePathFromMatch } from 'common/routing';
import ParticipantSubmitVoteForm from 'components/ParticipantSubmitVoteForm';

const handleSubmit = (payload, { props, setSubmitting, setErrors, resetForm }) => {
  const { title, length, description } = payload;
  const { participantId } = props;
  // TODO: make this a real date
  const submitDate = Date.now()/1000|0;
  props.setAgendaParticipantSubmitDate({ id: participantId, submitDate })
    .then((response) => {
      const basePath = basePathFromMatch(props.match);
      props.history.push(`${basePath}/confirm-submit`);
    }).catch((e) => {
      alert('error submitting');
      // setSubmitting(false)
      // setErrors({ email: ' ', password: ' ', form: errors })
    });
};

const ParticipantSubmitVoteFormWithFormik = withSemanticUIFormik({
  // mapPropsToValues: (vals) => {
  //   return ({
  //     title: null, length: null, description: null
  //   });
  // },
  displayName: "ParticipantSubmitVoteForm",
  handleSubmit: handleSubmit
})(ParticipantSubmitVoteForm);

const ParticipantSubmitVoteFormWithGraphQL = compose(
  graphql(schema.AgendaParticipant, {
    options: ({participantId}) => ({
      variables: {
        id: participantId
      }
    }),
    props: ({data: {agendaParticipant = {}} = {agendaParticipant: {}}}) => ({ submitDate: agendaParticipant.submitDate })
  }),
  graphql(schema.SetAgendaParticipantSubmitDate, {
    props: ({ mutate }) => ({
      setAgendaParticipantSubmitDate: (variables) =>
        mutate({variables})
    })
  })
)(ParticipantSubmitVoteFormWithFormik);

export default (withRouter(ParticipantSubmitVoteFormWithGraphQL));
