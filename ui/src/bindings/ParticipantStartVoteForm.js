import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose, graphql } from 'react-apollo';
import { withSemanticUIFormik } from 'common/form';
import * as schema from '../schema';
import { basePathFromMatch } from 'common/routing';
import ParticipantStartVoteForm from 'components/ParticipantStartVoteForm';

const handleSubmit = (payload, { props, setSubmitting, setErrors, resetForm }) => {
  const { title, length, description } = payload;
  const { participantId } = props;
  // TODO: make this a real date
  const startDate = Date.now()/1000|0;
  props.setAgendaParticipantStartDate({ id: participantId, startDate })
    .then((response) => {
      const basePath = basePathFromMatch(props.match);
      props.history.push(`${basePath}/rank`);
    }).catch((e) => {
      alert('error submitting');
      // setSubmitting(false)
      // setErrors({ email: ' ', password: ' ', form: errors })
    });
};

const ParticipantStartVoteFormWithFormik = withSemanticUIFormik({
  // mapPropsToValues: (vals) => {
  //   return ({
  //     title: null, length: null, description: null
  //   });
  // },
  displayName: "ParticipantStartVoteForm",
  handleSubmit: handleSubmit
})(ParticipantStartVoteForm);

const ParticipantStartVoteFormWithGraphQL = compose(
  graphql(schema.AgendaParticipant, {
    options: ({participantId}) => ({
      variables: {
        id: participantId
      }
    }),
    props: ({data: {agendaParticipant = {}} = {agendaParticipant: {}}}) => ({ startDate: agendaParticipant.startDate })
  }),
  graphql(schema.SetAgendaParticipantStartDate, {
    props: ({ mutate }) => ({
      setAgendaParticipantStartDate: (variables) =>
        mutate({variables})
    })
  })
)(ParticipantStartVoteFormWithFormik);

export default (withRouter(ParticipantStartVoteFormWithGraphQL));
