import { compose, graphql } from 'react-apollo';

import * as schema from '../schema';
import AllAgendas from 'components/AllAgendas';

export default compose(
  graphql(schema.AllAgendas, {
    props: (data) => {
      return ({
        agendas: data.data.allAgendas || []
      });
    }
  })
)(AllAgendas);
