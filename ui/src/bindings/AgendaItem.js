import React from 'react';
import { compose, graphql } from 'react-apollo';
import * as schema from '../schema';

import AgendaItem from 'components/AgendaItem';

export default compose(
  graphql(schema.DeleteAgendaItem, {
    props: ({mutate,
             ...rest}) => ({
               deleteAgendaItem: (agendaItemId) => {
                 return mutate({
                   variables: {
                     agendaItemId
                   }
                 }
                              );
               }
             })
  })
)(AgendaItem);
