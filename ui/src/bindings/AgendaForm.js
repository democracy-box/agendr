import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose, graphql } from 'react-apollo';
import { withSemanticUIFormik } from 'common/form';
import * as schema from '../schema';
import { basePathFromMatch } from 'common/routing';

import AgendaForm from 'components/AgendaForm';

const handleSubmit = (payload, { props, setSubmitting, setErrors, resetForm }) => {
  const { title, length, description } = payload;
  // TODO is this unneeded duplication?
  props.createAgenda({ title, length, description, variables: { title, length, description } })
    .then((response) => {
      resetForm();
      const {data: {createAgenda: {id}}} = response;
      // TODO include parent path in event this is nested (e.g. match.location.url)
      const basePath = basePathFromMatch(props.match);
      props.history.push(`${basePath}/${id}/add-participants`);
    }).catch((e) => {
      alert('error submitting');
      // setSubmitting(false)
      // setErrors({ email: ' ', password: ' ', form: errors })
    });
};

const AgendaFormWithFormik = withSemanticUIFormik({
  mapPropsToValues: (vals) => {
    return ({
      title: null, length: null, description: null
    });
  },
  displayName: "AgendaForm",
  handleSubmit: handleSubmit
})(AgendaForm);

const AgendaFormWithGraphQL = compose(
  graphql(schema.CreateAgenda, {
    props: ({ mutate }) => ({
      createAgenda: (variables) =>
        mutate({variables})
    })
  })
)(AgendaFormWithFormik);

export default withRouter(AgendaFormWithGraphQL);
