import { compose, graphql } from 'react-apollo';
import * as schema from '../schema';

import AgendaItemList from 'components/AgendaItemList';

export default compose(
  graphql(schema.AgendaWithResults, {
    options: ({agendaId}) => ({
      variables: {
        id: agendaId
      }
    }),
    props: ({data}) => {
      const {agenda={}} = data;
      const items = !data.loading && typeof agenda.items === 'object' ?
            agenda.items.concat()
            .sort((a, b) => (a.result.mean <= b.result.mean ? -1 : 1))
            : [];
      return ({
        // TODO: DRY this up, repeated from result report.
        // TODO: Refactor 'rank' calc to server, using IRV/Approval. (#21).
        // TODO: Switch calculation method (mean, median, rank) based on prop, instead of just using rank. See issue #22.
        meetingLength: agenda.length,
        loading: data.loading,
        items: (items || [])
      });
    }
  })
)(AgendaItemList);
