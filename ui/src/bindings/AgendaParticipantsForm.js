import React from 'react';
import { compose, graphql } from 'react-apollo';
import { withSemanticUIFormik } from 'common/form';
import * as schema from '../schema';

import AgendaParticipantsForm from 'components/AgendaParticipantsForm';

// const handleSubmit = ({email}, { props: {agendaId}, setSubmitting, setErrors, resetForm }) => {
const handleSubmit = (payload, { props, setSubmitting, setErrors, resetForm }) => {
  const { email } = payload;
  const { agendaId } = props;
  props.addAgendaParticipant({agendaId, email })
    .then((response) => {
      // props.data.refetch();
      resetForm();
      // props.history.push('/')
    }).catch((e) => {
      alert('error submitting');
      // setSubmitting(false)
      // setErrors({ email: ' ', password: ' ', form: errors })
    });
};

const AgendaParticipantsFormWithFormik = withSemanticUIFormik({
  mapPropsToValues: (vals) => {
    return ({
      email: null
    });
  },
  displayName: "AgendaParticipantsForm",
  handleSubmit: handleSubmit
})(AgendaParticipantsForm);

const AgendaParticipantsFormWithGraphQL = compose(
  graphql(schema.Agenda, {
    options: ({agendaId}) => ({
      variables: {
        id: agendaId
      }
    }),
    props: ({data: {agenda} = {agenda: {}}}) => ({agenda}) 
  }),
  graphql(schema.AddAgendaParticipant, {
    props: ({ mutate }) => ({
      addAgendaParticipant: (variables) => {
        return mutate({variables,
                       // TODO Refactor addAgendaParticipant to return agenda
                       // with new participant, remove refresh.
                       refetchQueries: [{
                         query: schema.Agenda,
                         variables: {id: variables.agendaId}
                       }],
                      }
                     );
      }
    })
  })
)(AgendaParticipantsFormWithFormik);

export default (AgendaParticipantsFormWithGraphQL);
