import React, { Component } from 'react';
import {
  withRouter,
  Route,
  Link
} from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container } from 'semantic-ui-react';

import ParticipantStartVoteForm from 'bindings/ParticipantStartVoteForm'

class ShowForm extends Component {
  render() {
    const {match} = this.props;
    const {params: {participantId} = {}} = match;
    return <SegmentGroup>
        <Segment>
          <ParticipantStartVoteForm participantId={participantId}/>
        </Segment>
      </SegmentGroup>
  }
}

export default withRouter(ShowForm);
