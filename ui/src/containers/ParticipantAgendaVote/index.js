import React, { Component } from 'react';
import {
  withRouter,
  Route,
  Link
} from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container } from 'semantic-ui-react';

import { basePathFromMatch } from 'common/routing'
import ShowForm from './ShowForm'
import ShowWelcome from './ShowWelcome'

import RankingForm from 'bindings/RankingForm'

class ParticipantAgendaVote extends Component {
  render() {
    const {match} = this.props;
    const participantId = match && match.params && match.params.participantId ? match.params.participantId : this.props.participantId;
    // If embedded in storybook, nested routers break due to match.path of '/',
    // which makes route path '//path'.
    const basePath = basePathFromMatch(match);
    return(
      <Container>
        <Route exact path={`${basePath}`} render={props =>
                                                  <Container>
                                                    Please follow a valid URL to vote.!
                                                  </Container>
                                                                }
        />
        <Route path={`${basePath}/:participantId`} exact component={ShowWelcome} />
        <Route path={`${basePath}/:participantId/rank`} component={ShowForm} />
        <Route path={`${basePath}/:participantId/confirm-submit`} render={props =>
                                                           <Container>
                                                             CONFIRMED!
                                                           </Container>
                                                          }
        />
        <Route path={`${basePath}/confirm-decline`} render={props =>
                                                           <Container>
                                                             DECLINED!
                                                           </Container>
                                                          }
        />
      </Container>
    );
  }
}

// export default ParticipantAgendaVote;
export default withRouter(ParticipantAgendaVote);
