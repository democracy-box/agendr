import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import {
  withRouter,
  Route,
  Link
} from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container } from 'semantic-ui-react';

import * as schema from '../../schema';
import Agenda from 'bindings/Agenda';
import ItemRankTable from 'bindings/ItemRankTable'
import ParticipantSubmitVoteForm from 'bindings/ParticipantSubmitVoteForm'

class ShowForm extends Component {
  render() {
    const {match, agenda, id} = this.props;
    const {params: {participantId} = {}} = match;
    return <SegmentGroup>
             {(agenda ?
               <Segment>
                 <Agenda agendaId={agenda.id}/>
               </Segment> : ''
              )}
             <Segment>
               <ItemRankTable participantId={id}/>
             </Segment>
             <Segment>
               <ParticipantSubmitVoteForm participantId={id}/>
             </Segment>
           </SegmentGroup>
  }
}

export default compose(
  graphql(schema.AgendaParticipant, {
    options: ({match: {params: {participantId}}}) => ({
      variables: {
        id: participantId
      }
    }),
    props: ({data,
             data: {
               agendaParticipant,
               agendaParticipant: {
                 id,
                 email,
                 agenda,
                 agenda: {
                   meetingLength
                 } = {},
                 agendaItemRankings
               } = {}
             } = {}
            }) => {
              return ({
                id,
                email,
                agenda,
                meetingLength,
                items: agendaItemRankings
              });
            }
  }),
)(withRouter(ShowForm));
