import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container, Button, Icon } from 'semantic-ui-react';

import AgendaSteps from 'bindings/AgendaSteps';
import Agenda from 'bindings/Agenda';
import ResultAgendaItemList from 'bindings/ResultAgendaItemList';
import ResultItemReport from 'bindings/ResultItemReport';

class AgendaCreateStartVote extends Component {
  render() {
    const { match } = this.props;
    const agendaId = match && match.params && match.params.agendaId ? match.params.agendaId : this.props.agendaId;
    return(
      <SegmentGroup>
        {/* TODO refactor so agendasteps lives in container and updates every step automatically. */}
        <AgendaSteps activeStep="5" agendaId={match.params.agendaId} />
        <Segment align="center" attached>
          <Container text>
            <p>See results.</p>
          </Container>
        </Segment>
        <Segment attached>
          <Agenda agendaId={agendaId} />
        </Segment>
        <Segment attached align="left">
          <ResultAgendaItemList agendaId={agendaId}/>
        </Segment>
        <Segment attached>
          <ResultItemReport agendaId={agendaId}/>
        </Segment>
      </SegmentGroup>
    );
  }
}

export default withRouter(AgendaCreateStartVote);
