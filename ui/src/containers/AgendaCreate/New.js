import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container } from 'semantic-ui-react';

import AgendaSteps from 'bindings/AgendaSteps';
import AgendaForm from 'bindings/AgendaForm';

class AgendaCreateNew extends Component {
  render() {
    return(
        <SegmentGroup>
          {/* TODO refactor so agendasteps lives in container and updates every step automatically. */}
          <AgendaSteps activeStep="1" />
          <Segment align="center" attached>
            <Container text>
              <p>Start by providing some context for the meeting. The meeting length will determine what agenda items are included or excluded for each ranking of the agenda items you'll enter in the next step.</p>
            </Container>
          </Segment>
          <Segment attached>
            <AgendaForm />
          </Segment>
        </SegmentGroup>
    );
  }
}

export default AgendaCreateNew;
