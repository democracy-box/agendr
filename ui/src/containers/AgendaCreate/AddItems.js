import React, { Component } from 'react';
import {
  withRouter,
  Link
} from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container, Button, Icon } from 'semantic-ui-react';

import AgendaSteps from 'bindings/AgendaSteps';
import Agenda from 'bindings/Agenda'
import AgendaItemForm from 'bindings/AgendaItemForm'

class AgendaCreateAddItems extends Component {
  render() {
    const {match, addAgendaItem} = this.props;
    return(
        <SegmentGroup>
        {/* TODO refactor so agendasteps lives in container and updates every step automatically. */}
        <AgendaSteps activeStep="3" agendaId={match.params.agendaId}/>
        <Segment align="center" attached>
        <Container text>
        <p>Add all items proposed for the agenda, along with their proposed times and a brief description of each.</p>
        </Container>
        </Segment>
        <Segment attached>
        <Agenda
          agendaId={match.params.agendaId}
          showItems
          showParticipants
        />
        </Segment>
        <Segment attached>
        <AgendaItemForm
          agendaId={match.params.agendaId}
        />
        </Segment>
        <Segment attached>
        <Button
          id='agendr-submit--agenda-add-items'
          icon
          primary
          fluid
          as={Link}
          to={`/${match.params.agendaId}/start-vote`}
          >
        Next: Start Vote <Icon name="arrow right" />
        </Button>
        </Segment>
        </SegmentGroup>
    );
  }
}

export default withRouter(AgendaCreateAddItems);
