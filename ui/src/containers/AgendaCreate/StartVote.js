import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container, Button, Icon } from 'semantic-ui-react';

import AgendaSteps from 'bindings/AgendaSteps';
import Agenda from 'bindings/Agenda';
import AgendaStartVoteForm from 'bindings/AgendaStartVoteForm';

class AgendaCreateStartVote extends Component {
  render() {
    const { match } = this.props;
    const agendaId = match && match.params && match.params.agendaId ? match.params.agendaId : this.props.agendaId;
    return(
      <SegmentGroup>
        {/* TODO refactor so agendasteps lives in container and updates every step automatically. */}
        <AgendaSteps activeStep="4" agendaId={match.params.agendaId} />
        <Segment align="center" attached>
          <Container text>
            <p>Finalize agenda and initiate voting.</p>
          </Container>
        </Segment>
        <Segment attached>
          <Agenda agendaId={agendaId} showItems showParticipants showParticipantVoteLinks/>
        </Segment>
        <Segment attached>
          <AgendaStartVoteForm agendaId={agendaId}/>
        </Segment>
        <Segment attached>
          <Button
            id='agendr-submit--agenda-see-results'
            icon
            primary
            fluid
            as={Link}
            to={`/${match.params.agendaId}/see-results`}
          >
            Next: See Results <Icon name="arrow right" />
          </Button>
        </Segment>
      </SegmentGroup>
    );
  }
}

export default withRouter(AgendaCreateStartVote);
