import React, { Component } from 'react';
import {
  withRouter,
  Route,
  Link
} from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container } from 'semantic-ui-react';

import AgendaForm from 'components/AgendaForm'
import AgendaCreateNew from 'containers/AgendaCreate/New'
import AgendaCreateAddParticipants from 'containers/AgendaCreate/AddParticipants'
import AgendaCreateAddItems from 'bindings/AgendaCreateAddItems'
import AgendaCreateStartVote from 'containers/AgendaCreate/StartVote'
// TODO fix this namespace, should not be "AgendaCreate"
import AgendaCreateResults from 'containers/AgendaCreate/Results'

class AgendaCreate extends Component {
  render() {
    const {match} = this.props;
    // If embedded in storybook, nested routers break due to match.path of '/',
    // which makes route path '//path'.
    const basePath = match && match.path && match.path !== "/" ? match.path : "";
    return(
        <Container>
        <Route exact path={basePath} component={AgendaCreateNew}/> 
        <Route path={`${basePath}/:agendaId/add-participants`} component={AgendaCreateAddParticipants}/> 
        <Route path={`${basePath}/:agendaId/add-items`} component={AgendaCreateAddItems}/> 
        <Route path={`${basePath}/:agendaId/start-vote`} component={AgendaCreateStartVote}/> 
        <Route path={`${basePath}/:agendaId/see-results`} component={AgendaCreateResults}/> 
        </Container>
    );
  }
}

// export default AgendaCreate;
export default withRouter(AgendaCreate);
