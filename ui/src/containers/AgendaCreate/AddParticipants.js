import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import { Segment, SegmentGroup, Container, Button, Icon } from 'semantic-ui-react';

import AgendaSteps from 'bindings/AgendaSteps';
import Agenda from 'bindings/Agenda';
import AgendaParticipantsForm from 'bindings/AgendaParticipantsForm';

class AgendaCreateAddParticipants extends Component {
  render() {
    const { match } = this.props;
    const agendaId = match && match.params && match.params.agendaId ? match.params.agendaId : this.props.agendaId;
    return(
      <SegmentGroup>
        {/* TODO refactor so agendasteps lives in container and updates every step automatically. */}
        <AgendaSteps activeStep="2" agendaId={match.params.agendaId} />
        <Segment align="center" attached>
          <Container text>
            <p>Add one or more participants by email. Participants will be able to submit rankings to vote on the final agenda.</p>
          </Container>
        </Segment>
        <Segment attached>
          <Agenda agendaId={agendaId} showParticipants/>
        </Segment>
        <Segment attached>
          <AgendaParticipantsForm agendaId={agendaId}/>
        </Segment>
        <Segment attached>
          <Button
            id='agendr-submit--agenda-add-participants'
            icon
            primary
            fluid
            as={Link}
            to={`/${match.params.agendaId}/add-items`}
          >
            Next: Add Items <Icon name="arrow right" />
          </Button>
        </Segment>
      </SegmentGroup>
    );
  }
}

export default withRouter(AgendaCreateAddParticipants);
